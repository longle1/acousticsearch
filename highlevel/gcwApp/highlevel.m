% High level component of the acoustic search system
% 
% The high-level app periodically polls the RAB server with a GET, after
% which the server responds with event data, if available. Two callbacks
% are used to 1) invoke GET command, and 2) package event data into
% appropriate data queues.
%
% The main loop is responsible for the actual bayesian processing/low-high 
% fusion, which is executed only after a complete burst is received.
%
% David Jun
% University of Illinois
% 05/08/2014
%
% Add contract setup with the RAB
% 
% Long Le, longle1@illinois.edu
% University of Illinois
% 05/18/2014
%

clear all; close all;
addpath('../common');
%{
old = cd('../../pmtk/');
initPmtk3
cd(old);
%}
% Clear deprecated timer or instr object
delete(timerfind);
delete(instrfind);
%% Parameters


% Configure timer
timerGET = timer('TimerFcn',@timerGET_cb, 'Period', 5, 'ExecutionMode','fixedRate');
global pktQ burstQ
%pktQ = CQueue();
pktQ = [];
burstQ = CQueue();

% Configure TCPIP
global recvTCP
recvTCP=tcpip('localhost', 8086);
recvTCP.InputBufferSize = 2^16;
recvTCP.BytesAvailableFcnMode = 'terminator';
recvTCP.Terminator = 'LF';
recvTCP.BytesAvailableFcn = @recvTCP_cb;

%% Processing parameters
% load high-level prior knowledge
load pDS;
% audio bands of interest
bnd = [9];
% maximum burst duration that is considered. this is brittle. next
% implementation should be a rolling window, similar to Doug's ridge
% building implementation
T = pDS.t(end);

%% Main

% DEBUG used to observe state machine in recvTCP_cb.m
global statemachine timemachine
statemachine = [];
timemachine = [];

% connect to server
fopen(recvTCP);
% call GET at fixed rate
start(timerGET);

fprintf(recvTCP, ['POST /rab/user/contract?userid=0&contractid=0&avgErate=1&price=100 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response
fprintf(recvTCP, ['POST /rab/user/control?userid=0&enable=true HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response
fprintf(recvTCP, ['POST /rab/user/stream?userid=0&contractid=0 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response

done = false;
idx = 0;
while(~done)
    if(~isempty(burstQ))    % burst of data is available to process
         % each burst is represented by a packet queue
        cur_pktQ = burstQ.pop();
            
        % ============= PROCESS THE DATA BURST ===============
        y = nan*zeros(T,1);
        n = nan*zeros(T,1);

        % most recent packet time
%        en = cur_pktQ.back().time;
        en = cur_pktQ(end).time;

        % oldest packet
%        pkt = cur_pktQ.pop();
        pkt = cur_pktQ(1);
        cur_pktQ = cur_pktQ(2:end);
        st = pkt.time;

        if(en-st+1 > T) % burst is longer than what we want
            % do a heuristic... truncate
            en = st+T-1;
        end
        k=0;
        for kk=st:en
            k=k+1;
            if(pkt.time == kk)
                % extract signal power
                y(k) = pkt.sig;
                % extract noise power
                n(k) = pkt.noise;
                
                if(~isempty(cur_pktQ))
                    % pop packet off of queue
%                    pkt = cur_pktQ.pop();
                    pkt = cur_pktQ(1);
                    cur_pktQ = cur_pktQ(2:end);
                end
            end
        end
%        cur_pktQ.empty();
        cur_pktQ = [];

        % estimate parameters for F0 and F1 Exp distributions
        l0 = 1/mean(n(~isnan(n)));
        l1 = 1/mean(y(~isnan(y)));

        % fill erasures with last known value... best guess
        for k=1:T
            if(isnan(n(k)))
                y(k) = n(k-1);
                n(k) = n(k-1);
            end
        end

        % evaluate at durations of interest to application
        pYD = zeros(length(pDS.t),1);
        for k=1:length(pDS.t)
            t = pDS.t(k);                      
            % compute log P(y^T | duration=t)
            lpYD = t*log(l1) + (T-t)*log(l0) ...
                   - sum(y(1:t))*l1 - sum(y(t+1:T))*l0;
            % for numerical issues
            pYD(k) = exp(lpYD);
        end
        pYD = mkStochastic(pYD);
        
        % increment burst count
        idx = idx+1;
%        stem(pYD);
        m(idx) = pDS.t' * pYD;
        v(idx) = ((pDS.t-m(idx)).^2)' * pYD;
        snr(idx) = l0/l1;
        display(sprintf('index: %i, SNR: %f, spread: %f',idx, l0/l1, v(idx)));
%        pause;  

        % ============= END OF PROCESSING  ===============
    end
end

% close server
fclose(recvTCP);
stop(timerGET);