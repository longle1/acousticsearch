% recvTCP_cb.m
%
% triggered when server responds to GET request.
%
% A server response is processed here, which packages received events into
% a packet queue, and creates a new pktQ for each burst of events. These 
% pktQs are placed into a burstQ. The algorithm for segmenting bursts is a 
% heuristic and tailor-fit for this data trace. Modifications may be 
% required for other scenarios.
%
% David Jun
% University of Illinois
% 05/08/2014
%
% [DJ 05/25/2014]: Added hacky code to check for any server response. In 
% calling code, execution should block for a short time (e.g., 1 sec) 
% before checking negotiationStatus.
%
function recvTCP_cb(obj, event)

global data

% manually declare end of burst after 500ms.
WATCHDOG = 0.500;
HANGOVER = 1;

global pktQ burstQ c1 

global statemachine timemachine

global negotiationStatus

if(negotiationStatus == 0)
    negotiationStatus = 1;
end

data = fscanf(obj);
disp(data);
if (data(1)=='[' && data(2)=='{')
    data = parse_json(data); %data(end-1:end) are two lf/10

    for k =1:numel(data)
        pkt = [];
        pkt.freq = str2double(data{k}.freq);
        pkt.time = str2double(data{k}.time);
        pkt.sig = str2double(data{k}.sig);
        pkt.noise = str2double(data{k}.noise);

        % local time stamp (for watchdog)
        c1 = clock;

        if(~isempty(pktQ))
%            if(pkt.time - pktQ.back().time - 1 <= HANGOVER)
            if(pkt.time - pktQ(end).time - 1 <= HANGOVER)
                statemachine = [statemachine, 1];
                timemachine = [timemachine, pkt.time];

%                pktQ.push(pkt);
                pktQ = [pktQ, pkt];
            else % new burst
                statemachine = [statemachine, 2];
                timemachine = [timemachine, pkt.time];
                
                % finish last burst
                burstQ.push(pktQ);

                % start new pktQ
%                pktQ.empty();
%                pktQ.push(pkt);
                pktQ = pkt;
            end
        else
            statemachine = [statemachine, 3];
            timemachine = [timemachine, pkt.time];
            
            % beginning of a burst
            pktQ = pkt;
%            pktQ.push(pkt);
        end
    end
else
    % if enough time has passed from last pkt, burst is over
    if(~isempty(pktQ))
        c2 = clock;
        if(etime(c2,c1) > WATCHDOG)
            statemachine = [statemachine, 4];
            timemachine = [timemachine, 0];
                
            burstQ.push(pktQ);
%            pktQ.empty();
            pktQ = [];
        else
            statemachine = [statemachine, 5];
            timemachine = [timemachine, 0];
        end
    else
        statemachine = [statemachine, 0];
        timemachine = [timemachine, 0];
    end
end
    

end