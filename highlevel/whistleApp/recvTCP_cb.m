% recvTCP_cb.m
%
% triggered when server responds to GET request.
%
% A server response is processed here, which packages received events into
% a packet queue, and creates a new pktQ for each burst of events. These 
% pktQs are placed into a burstQ. The algorithm for segmenting bursts is a 
% heuristic and tailor-fit for this data trace. Modifications may be 
% required for other scenarios.
%
% David Jun
% University of Illinois
% 05/08/2014
%
% [DJ 05/25/2014]: Added hacky code to check for any server response. In 
% calling code, execution should block for a short time (e.g., 1 sec) 
% before checking negotiationStatus.
%
function recvTCP_cb(obj, event)

data = fscanf(obj);
disp(data);

global negotiationStatus

if(negotiationStatus == 0)
    negotiationStatus = 1;
end
    

end