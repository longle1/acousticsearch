% Whistle detection App.
%
% Long Le
% University of Illinois
%

clear all; close all;
addpath(genpath('..\..\common'));
addpath(genpath('../../../IlliadAccess/matlab'))
%import java.lang.System
% Import cert into Matlab jvm truststore.
% Need write access to the truststore (cacerts)
%importcert('C:\Users\Long\Desktop\Projects\mongoser\config\jetty.crt')  

%% Periodically poll database for events

BRIDGEIP='128.174.210.208';
%BRIDGEIP='192.168.8.112';
USERID='760f6fe5759c473d26b123f29e86b';
LIGHTID='2';
%LIGHTID='1';

DB = 'publicDb';
USER = 'publicUser';
PWD = 'publicPwd';

load whistleTemp0.mat whistleTemp0
load whistleTemp1.mat whistleTemp1
toggle = false;
state = 0;
period = 1.0;% in second
lastTime = now;
while(1)
    %try
        disp('polling...')
        pause(period);
        
        % periodic query
        q.t1 = lastTime+1/864000; q.t2 = now;
        events = IllQueryEvent(DB, USER, PWD, q);
        if (~iscell(events))
            continue;
        end
        
        disp('Found data, start processing...')
        for k = 1:numel(events)
            lastTime = datenum8601(events{k}.recordDate.x0x24_date)-5/24; % last acquired file, local time

            if (state == 0)
                fdnames = fieldnames(events{k}.TFRidge);
                for l = 1:numel(fdnames)
                    ridge = events{k}.TFRidge.(fdnames{l}).freq;
                    
                    % Detect whistle command
                    score = zeros(1, numel(whistleTemp0));
                    for m = 1:numel(whistleTemp0)
                        score(m) = dpscore(whistleTemp0{m}, ridge);
                    end
                    fprintf(1, 'Score :%5.3f\n', min(score));

                    if (min(score) < 80)
                        disp('State = 1')
                        state = 1;
                        break;
                    end
                end
            else % state == 1
                fdnames = fieldnames(events{k}.TFRidge);
                for l = 1:numel(fdnames)
                    ridge = events{k}.TFRidge.(fdnames{l}).freq;
                    
                    % Detect whistle command
                    score = zeros(1, numel(whistleTemp1));
                    for m = 1:numel(whistleTemp1)
                        score(m) = dpscore(whistleTemp1{m}, ridge);
                    end
                    fprintf(1, 'Score :%5.3f\n', min(score));
                    
                    if (min(score) < 500)
                        disp('Whistle detected!!!')
                        toggle = ~toggle;
                        % Post to the light
                        if (toggle)
                            disp('On');
                            lightOn(BRIDGEIP, USERID, LIGHTID, true);
                        else
                            disp('Off');
                            lightOn(BRIDGEIP, USERID, LIGHTID, false);
                        end
                        break;
                    end
                end
                % Reset
                disp('Reset state = 0')
                state = 0;
            end
        end    
    %catch e
    %    disp('error');
    %end
end