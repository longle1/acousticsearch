% whistleSwarmletApp.m
%
% The main swarmlet code. It launches the user GUI and also runs the
% whistle-detection algorithm, which is driven by incoming UDP events.
% 
% The user manually (via the GUI) creates a connection to the service, and
% negotiates a contract. Once data is confirmed to be streaming, events 
% will arrive via UDP. The UDP callback will automatically parse the data
% and put new events in a queue. A main [infinite] while loop will 
% constantly monitor for new packets, which will trigger the time-frequency
% whistle-detection algorithm to compute its test statistic. Data and
% results will be displayed in the GUI figures, accordingly.
%
% David Jun
% University of Illinois
% 05/27/2014
%

clear all; close all;
addpath('../../common');

% Clear deprecated timer or instr object
delete(timerfind);
delete(instrfind);

%% network parameters

% IP addresses can be adjusted by the user via the GUI. 
global serviceIP swarmletIP

% default ip address for the low-level service
serviceIP = '172.16.221.226';
% my default ip address. needed for the service to post UDP events to me.
swarmletIP = '130.126.122.126';


%% detection-algorithm parameters

% parameters can be adjusted by the user via the GUI
global whistleBW detthresh

% default bandwidth around max frequency (assumed to be the whistle)
whistleBW = 20;
% default detection threshold
detthresh = 100;

% parameters will be set by Accessor, after contract is settled
global binmin binmax noise
% default minimum frequency bin to track
binmin = [];
% default maximum frequency bin to track
binmax = [];
% keep track of noise profile
noise = [];

%% book keeping variables

global DEBUG
DEBUG = 0;

% figure handles for displaying incoming events and detection statistics
global tbEvt evtBuf totEvtCt axStat statBuf statTime

% used to display in GUI. initialize to 0. will auto-init in timer cb.
totEvtCt = 0;
% set up timer to report events periodically
timerDisp = timer('TimerFcn',@timerDisp_cb, 'Period', 1.0, 'ExecutionMode','fixedRate');

% a pktQ has events belonging to a single time frame. burstQ holds pktQs.
global frameQ evtQ
frameQ = [];
evtQ = CQueue();

% global tbEvt
% 
% % Create a uicontrol of type "text"
% axes(hObject);
% tbEvt = uicontrol('style','text');
% set(tbEvt,'Units','characters')
% % To move the the Text Box around you can set and get the position of Text Box itself
% mTextBoxPosition = get(tbEvt,'Position');
% % position has four elements [x y length height]
% set(tbEvt, 'Position', [1, 1, 50, 30]);

%% Main

% launch user GUI. nothing interesting will happen in main loop without
% user input
whistleSwarmletGUI;

% start display timer
start(timerDisp);

%%
% initialize processing variable
processnow = 0;
% flush evtQ
evtQ.empty();
% loop forever. this is just the detection algorithm. results will be
% displayed in GUI
while(1)
    % ================== GROUP EVENTS BY TIME ==================
    if(~isempty(evtQ))
        if(isempty(frameQ))
            % create queue
            frameQ = evtQ.pop();
        else
            pkt = evtQ.pop();
            curtime = frameQ(1).time;
            if(pkt.time == curtime)
                % push onto queue
                frameQ = [frameQ, pkt];
            else
                % assumes events arrive sequentially. MAY NOT BE TRUE!
                % all events for current time frame are now available.
                cur_frameQ = frameQ;
                % cur_frameQ will be procesed in this iteration. reset Q.
                frameQ = pkt;
                
                % set flag to run detection algorithm
                processnow = 1;
            end
        end
    else
        if(~isempty(frameQ))   % all evts in Q accounted for. process frame
            processnow = 1;
            cur_frameQ = frameQ;
            curtime = cur_frame(1).time;
            frameQ = [];
        end
    end
    % ================= END OF GROUPING TASK ===================
    
    
    % ================ PROCESS THE TIME FRAME ==================
    if(processnow == 1)
        processnow = 0;
        evtCt = length(cur_frameQ);

%         % by default, fill frame with last known noise values
%         frame = noise;
% 
%         % process each event. from RAB, bins will automatically be in desired range
%         for i=1:evtCt
%             pkt = cur_frameQ(i);
%             frame(pkt.freq) = pkt.sig;
%             noise(pkt.freq) = pkt.noise;
%         end
        cur_frameQ = [];
%
%         % we can do better, but this is consistent w/ centralized version
%         [val, idx] = max(frame);
%         if(idx>=binmin && idx<=binmax)
%             ids = binmin:binmax;
%             st = idx-whistleBW/2;
%             en = idx+whistleBW/2;
%             z = val / mean(frame(ids(~(ids>=st&ids<=en))));
%         else
%             z = 0;
%         end
%         
% 
%         % update detection statistic plot
%         statTime = [statTime(2:end); curtime];
%         statBuf = [statBuf(2:end); z];
%         axis([statTime(1), statTime(end), 0 12000]);
%         refreshdata(fhStat);

        % update event buffer display
        totEvtCt = totEvtCt + evtCt;
    end
    
    drawnow;
    % ================== END OF PROCESSING  =====================
end


