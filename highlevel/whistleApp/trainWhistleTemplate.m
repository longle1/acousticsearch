% Train whistle templates
%
% Long Le
% University of Illinois
%

clear all; close all;
addpath(genpath('..\..\common'));
addpath(genpath('../../../IlliadAccess/matlab'))

%% Get data and event from the server
DB = 'publicDb';
USER = 'publicUser';
PWD = 'publicPwd';

%q.t1 = datenum(2014,8,12,16,11,0); q.t2 = datenum(2014,8,12,16,30,0);
q.t1 = datenum(2014,10,05,19,55,00); q.limit = 24;
events = IllView(DB, USER, PWD, q);

%% Hand pick template ridges
%{
temp = 1;
if (temp)
    eventIdx = [7 9 11 15 16 22]; 
    ridgeIdx = {'x0x33_' 'x0x30_' 'x0x30_' 'x0x30_' 'x0x31_' 'x0x30_'};
else
    eventIdx = [3 6 8 10 12 24]; 
    ridgeIdx = {'x0x32_' 'x0x30_' 'x0x31_' 'x0x31_' 'x0x30_' 'x0x31_'};
end
whistleTemp = cell(1, numel(eventIdx));
time = cell(1, numel(eventIdx));
for k = 1:length(eventIdx)
    time{k} = events{eventIdx(k)}.TFRidge.(ridgeIdx{k}).time;
    whistleTemp{k} = events{eventIdx(k)}.TFRidge.(ridgeIdx{k}).freq;
end
for k = 1:length(whistleTemp)
    figure; plot(time{k}, whistleTemp{k}); ylim([1 256]);
end
if (temp)
    save whistleTemp1.mat whistleTemp
else
    save whistleTemp0.mat whistleTemp
end
%}