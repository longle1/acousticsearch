function recvUDP_cb(obj, event)

global evtQ DEBUG

data = fread(obj, obj.BytesAvailable);
data = char(data');

if(DEBUG)
    disp(data);
end

if (data(1)=='{')
    data = parse_json(data); %data(end-1:end) are two lf/10
    
    pkt = [];
    pkt.freq = str2double(data.freq);
    pkt.time = str2double(data.time);
    pkt.sig = str2double(data.sig);
    pkt.noise = str2double(data.noise);
        
    evtQ.push(pkt);
end

end