% timerGET_cb.m
%
% triggered periodically to request for more event data. does not wait for
% a response. a response is handled by the tcpip callback.
%
% David Jun
% University of Illinois
% 05/08/2014
%

function timerDisp_cb(obj, event)

global totEvtCt tbEvt evtBuf

newevt = sprintf('%d events arrived.',totEvtCt);
totEvtCt = 0;
evtBuf = {evtBuf{2:end}, newevt};
set(tbEvt,'String',evtBuf);

end