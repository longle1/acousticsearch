#!/bin/bash

APIKEY=3effac5ef532418e8b1753c18d97e9de
APISECRET=9e11bf43a5a9419b9839291a8950cd43
KEY=key1
STORE=true

if [ $STORE = true ]; then
	echo 'store'
	# POST by default with -d 
	curl -u $APIKEY:$APISECRET \
		-d '{"key": "'$KEY'"}' \
		'https://api.tempo-db.com/v1/series'
	
	for i in `seq 1 10`;
	do
		curl -u $APIKEY:$APISECRET \
			-d '[{"t":"'`date +%Y-%m-%dT%H:%M:%S.%NZ`'","v":'$i'}]' \
			'https://api.tempo-db.com/v1/series/key/'$KEY'/data'
	done		

	# GET by default
	curl -u $APIKEY:$APISECRET \
		'https://api.tempo-db.com/v1/series/key/'$KEY'/segment?start=2014-06-23&end=2014-06-27&tz=America/Chicago'
else
	echo 'clean'
	curl -X DELETE \
		-u $APIKEY:$APISECRET\
		'https://api.tempo-db.com/v1/series?key='$KEY
fi
