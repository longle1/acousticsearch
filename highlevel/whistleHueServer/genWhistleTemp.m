% Develop principled approach to detect whistle using ridge descriptor data
% in the fs folder
%
% Long Le
% University of Illinois
%

clear all; close all;
addpath('../../common');

%% Read all desc data
file = dir('./fs/*.mat');
numfile = numel(file);
alldat = cell(1,0);
idx = 0;
for k = 1:numfile
    load(['./fs/' file(k).name]);
    f = fieldnames(data);
    for l = 1:numel(f)
        lf = cell2mat(data.(f{l}).lowfreq);
        hf = cell2mat(data.(f{l}).highfreq);
        
        if (numel(lf) > 10)
            idx = idx+1;
            alldat{idx} = [lf;hf];
        end
    end
end

subperfig = 36; % # of subplots per figure
linperfig = 3; % # of lines per figure
for k = 1:numel(alldat)
    figure(ceil(k/subperfig))
    subplot(linperfig,ceil(subperfig/linperfig),mod(k-1, subperfig)+1); 
    plot(alldat{k}')
    axis([1 size(alldat{k},2) 1 256])
    title(sprintf('%d',k))
end

%% Manually select whistling ridges by indices
lidx = [3 5 8 41 47 72 73 75 76 86 88 91 102 117];
whistleTemp = cell(1,0);
for k = 1:numel(lidx)
    whistleTemp{k} = mean(alldat{lidx(k)});
end

figure
for k = 1:numel(whistleTemp)
    subplot(1,numel(whistleTemp),k); 
    plot(whistleTemp{k}')
    axis([1 size(whistleTemp{k},2) 1 256])
    title(sprintf('%d',lidx(k)))
end

save whistleTemp.mat whistleTemp

%% Evaluate performance of each template within the dataset
score = zeros(numel(whistleTemp), size(alldat,2));
for k = 1:numel(whistleTemp)
    for l = 1:size(alldat,2)
        score(k,l) = dpscore(whistleTemp{k}, mean(alldat{l}));
    end
end
%figure; stem(score(3,:))
figure; imagesc(score)