% Whistle to Hue Server
%
% Long Le, longle1@illinois.edu
% University of Illinois
%

clear all; close all;
if exist('C:\\cygwin64', 'dir') == 7
    DIR='C:\\cygwin64';
elseif exist('C:\\cygwin', 'dir') == 7
    DIR='C:\\cygwin';
else
    error('Cygwin is not installed')
end

addpath(genpath('..\..\common'));
javaaddpath '.workspace/WorkerThread/bin/'

% Clear deprecated timers and instrs
delete(timerfind); delete(instrfind);

%% Setup
% Configure tcpip server
import java.net.ServerSocket
import java.io.*
localSocket = ServerSocket(8086);
localSocket.setSoTimeout(30000);

toggle = false;
load whistleTemp.mat whistleTemp

%% Main

while(1)
    try
        remoteSocket = localSocket.accept();
        fprintf(1, '*** Client connected\n');

        % Response
        output_stream = remoteSocket.getOutputStream;
        d_output_stream = DataOutputStream(output_stream);
        
        message = sprintf('HTTP/1.1 200 OK\n');
        %fprintf(1, '*** Writing %s', message)
        %fprintf(1, '*** Writing %d bytes\n', length(message))
        d_output_stream.writeBytes(char(message));
        d_output_stream.flush;
        
        % Read request
        input_stream   = remoteSocket.getInputStream;
        d_input_stream = DataInputStream(input_stream);
        
        header = zeros(1, 4096, 'uint8');
        header(1) = d_input_stream.readByte;
        header(2) = d_input_stream.readByte;
        k = 2;
        while(1)
            k=k+1;
            header(k) = d_input_stream.readByte;
            if (header(k) == 10 && header(k-2) == 10) % endline is \r\n
                break;
            end
        end
        header = char(header);
        idx = strfind(header, 'Content-Length');
        token = textscan(header(idx:end), '%s %s', 'delimiter', ':');
        bodylen = str2double(token{2}{1});
        fprintf(1, '*** Reading %d bytes\n', bodylen);

        body = zeros(1, bodylen, 'uint8');
        for k = 1:bodylen
            body(k) = d_input_stream.readByte;
        end
        body = char(body);
        
        %disp(header)
        %disp(body)
        data = parse_json(body);
        % BEWARE: saving data slow down server responsiveness significantly
        save(sprintf('./fs/%s.mat', datestr8601(clock,'ymdHMS3')), 'data');
        %{
        fdata = formatter(data);
        fdata(fdata==9 | fdata == 10) = []; % remove tab and newline
        start(WorkerThread('tmp.json',fdata, [DIR '\\bin\\bash xivelyWrite.sh tmp.json']));
        %}
        
        f = fieldnames(data);
        for l = 1:numel(f)
            lf = cell2mat(data.(f{l}).lowfreq);
            hf = cell2mat(data.(f{l}).highfreq);

            % Process each ridge now
            
            % Detect whistle command
            ridge = mean([lf;hf]);
            score = zeros(1, numel(whistleTemp));
            for m = 1:numel(whistleTemp)
                score(m) = dpscore(whistleTemp{m}, ridge);
            end

            %fprintf(1, 'Score :%d\n', max(score));
            if (min(score) < 300)
                disp('Whistle detected!!!')
                toggle = ~toggle;
                % Post to the light
                if (toggle)
                    system([DIR '\\bin\\bash lightOn.sh true'])
                else
                    system([DIR '\\bin\\bash lightOn.sh false'])
                end
            end
            
            % Detect tone control
            %{
            if var(hf-lf) < 30 && all(diff(ridge)>=0) && length(ridge) > 15
                disp('Hue controlled!!!')
                huepts = min(ridge):(max(ridge)-min(ridge))/3:max(ridge);
                for m = 1:numel(huepts)
                    system(sprintf([DIR '\\bin\\bash lightHue.sh %d'], round(huepts(m)/256*2^16)))
                end
            end
            %}
        end
        
        fprintf(1, '*** =================\n');
        remoteSocket.close;
    catch e
        disp('error or timeout');
    end
end

localSocket.close;