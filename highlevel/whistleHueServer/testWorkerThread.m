clear all; close all

if exist('C:\\cygwin64', 'dir') == 7
    DIR='C:\\cygwin64';
elseif exist('C:\\cygwin', 'dir') == 7
    DIR='C:\\cygwin';
else
    error('Cygwin is not installed')
end

system(['"C:\Program Files\Java\jdk1.7.0_21\bin\javac"' ' -target 1.6 -source 1.6 .workspace\WorkerThread\src\WorkerThread.java'])
movefile('.workspace/WorkerThread/src/WorkerThread.class', '.workspace/WorkerThread/bin/WorkerThread.class');

tic
data = 'Hello coco';  % pre-processing, 5M elements, ~40MB

%fid = fopen('test.data','w');
%fwrite(fid,data,'double');
%fclose(fid);

javaaddpath '.workspace/WorkerThread/bin/'
start(WorkerThread('tmp.json',data, [DIR '\\bin\\bash -c ls']));

disp(data)
toc