import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;


public class WorkerThread extends Thread{
	String filename;
	String cmd;
    char[] data;
    public WorkerThread(String filename, char[] data, String cmd)
    {
        this.filename = filename;
        this.data = data;
        this.cmd = cmd;
    }
    @Override
    public void run()
    {
        try
        {
            DataOutputStream out = new DataOutputStream(
                                     new FileOutputStream(filename));
            for (int i=0; i < data.length; i++)
            {
                out.writeByte(data[i]);
            }
            out.close();
            
            //Process p = Runtime.getRuntime().exec(new String[]{"C:\\cygwin64\\bin\\bash","-c","ls"});
            Process p = Runtime.getRuntime().exec(cmd);
            BufferedReader in = new BufferedReader(  
                                new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {  
                System.out.println(line);
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }
}
