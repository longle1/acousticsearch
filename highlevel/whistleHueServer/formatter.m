function fdata = formatter(data)
% Format data to be added into the database
%
% Long Le
% University of Illinois
%

f = fieldnames(data);
channel = cell(1, numel(f));
for l = 1:numel(f)
    lf = cell2mat(data.(f{l}).lowfreq);
    hf = cell2mat(data.(f{l}).highfreq);
    
    datpts = cell(1, numel(lf));
    currTime = clock;
    for k = 1:numel(lf)
        currTime(end) = currTime(end)+0.001;
        datpts{k} = struct('at', datestr8601(currTime,'*ymdHMS3'),'value', sprintf('[%d, %d]',lf(k), hf(k)));
    end
    datpts = cell2mat(datpts);
    channel{l} = struct('id', sprintf('%d', l), 'datapoints', datpts);
end
fdata = savejson('datastreams',channel);

end