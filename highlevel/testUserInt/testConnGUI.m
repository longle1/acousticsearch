function varargout = testConnGUI(varargin)
% TESTCONNGUI MATLAB code for testConnGUI.fig
%      TESTCONNGUI, by itself, creates a new TESTCONNGUI or raises the existing
%      singleton*.
%
%      H = TESTCONNGUI returns the handle to a new TESTCONNGUI or the handle to
%      the existing singleton*.
%
%      TESTCONNGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTCONNGUI.M with the given input arguments.
%
%      TESTCONNGUI('Property','Value',...) creates a new TESTCONNGUI or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before testConnGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to testConnGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help testConnGUI

% Last Modified by GUIDE v2.5 27-May-2014 15:48:08

% lookup table for prices. bandIDx on rows, thresh on cols

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @testConnGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @testConnGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before testConnGUI is made visible.
function testConnGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to testConnGUI (see VARARGIN)
addpath('../../common');
% Choose default command line output for testConnGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, false);

% UIWAIT makes testConnGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = testConnGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the negotiate flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to negotiate the data.
if isfield(handles, 'mydata') && ~isreset
    return;
end

handles.mydata.PRICELOOKUP = [10, 20, 30;
                              40, 50, 60;
                              70, 80, 90;
                              100, 110, 120;
                              130, 140, 150];

set(handles.price,'String',sprintf('$%d',handles.mydata.PRICELOOKUP(1,1)));

global serviceIP swarmletIP
if(~isempty(serviceIP))
    handles.mydata.serviceuri = serviceIP;
else
    handles.mydata.serviceuri = 'localhost';
end
handles.mydata.serviceport = 8086;
handles.mydata.thresh = 1;
handles.mydata.bandIDx = 1;
handles.mydata.lifestyle = 1;
if(~isempty(swarmletIP))
    handles.mydata.streamuri = swarmletIP;
else
    handles.mydata.streamuri = '127.0.0.1';
end
handles.mydata.streamport = 8089;

set(handles.serviceuri,'String',handles.mydata.serviceuri);
set(handles.serviceport,'String',num2str(handles.mydata.serviceport));
set(handles.connect,'String','Connect');

set(handles.freqbands,'Value',handles.mydata.bandIDx);
set(handles.freqbands,'Enable','off');
set(handles.freqbands,'String',{'Up to 125Hz'});
set(handles.detthreshold, 'Value', handles.mydata.thresh);
set(handles.detthreshold, 'Enable', 'off');
set(handles.lifestyle,'Value',1);
set(handles.lifestyle,'String',{'Poor','Middle-class','Rich'});
set(handles.lifestyle, 'Enable', 'off');
set(handles.negotiate,'String','Negotiate');
set(handles.negotiate,'Enable','off');

set(handles.streamtype,'Enable','off');
set(handles.streamuri,'String',handles.mydata.streamuri);
set(handles.streamuri,'Enable','off');
set(handles.streamport,'String',num2str(handles.mydata.streamport));
set(handles.streamport,'Enable','off');
set(handles.stream,'String','Stream');
set(handles.stream,'Enable','off');

% Update handles structure
guidata(handles.figure1, handles);


% --- Executes on button press in connect.
function connect_Callback(hObject, eventdata, handles)
% hObject    handle to connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global recvTCP

if(strcmp(get(hObject,'String'),'Connect'))
    recvTCP=tcpip(handles.mydata.serviceuri, handles.mydata.serviceport);
    recvTCP.InputBufferSize = 2^16;
    recvTCP.BytesAvailableFcnMode = 'terminator';
    recvTCP.Terminator = 'LF';
    recvTCP.BytesAvailableFcn = @recvTCP_cb;
    fopen(recvTCP);
    if(strcmp(recvTCP.status, 'open'))
        set(hObject,'String','Disconnect');
        set(handles.serviceuri,'Enable','off');
        set(handles.serviceport,'Enable','off');
        
        set(handles.lifestyle,'Enable','on');
        set(handles.freqbands,'Enable','on');
        set(handles.detthreshold, 'Enable', 'on');
        set(handles.negotiate,'Enable','on');
    else
        fclose(recvTCP);
        delete(recvTCP);
    end
else
    delete(instrfind);
    set(hObject,'String','Connect');
    set(handles.serviceuri,'Enable','on');
    set(handles.serviceport,'Enable','on');
    
    set(handles.lifestyle,'Value',1);
    set(handles.lifestyle,'String',{'Poor','Middle-class','Rich'});
    set(handles.lifestyle,'Enable','off');
    set(handles.freqbands,'Value',1);
    set(handles.freqbands,'String',{'Up to 125Hz'});
    set(handles.freqbands,'Enable','off');
    set(handles.detthreshold, 'Enable', 'off');
    set(handles.negotiate,'String','Negotiate');
    set(handles.negotiate,'Enable','off');
    
    set(handles.streamtype,'Enable','off');
    set(handles.streamuri,'Enable','off');
    set(handles.streamport,'Enable','off');
    set(handles.stream,'String','Stream');
    set(handles.stream,'Enable','off');
end


% --- Executes on button press in negotiate.
function negotiate_Callback(hObject, eventdata, handles)
% hObject    handle to negotiate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global recvTCP negotiationStatus

tmp = get(handles.price,'String');
price = floor(str2double(tmp(2:end)));
rate = floor(price / 10);
userid = 0;
contractid = 0;
negotiationStatus = 0;
msg = sprintf('POST /rab/user/contract?userid=%d&contractid=%d&avgErate=%d&price=%d HTTP/1.0\nConnection: Keep-Alive\n\n',...
        userid, contractid, rate, price);
fprintf(recvTCP, msg);
fprintf(1,'BID (userid: %d, contractid: %d):\trateid: %d\tprice: $%d\n',userid,contractid,rate,price);
pause(1);
msg = sprintf('POST /rab/user/control?userid=%d&enable=true HTTP/1.0\nConnection: Keep-Alive\n\n',userid);
fprintf(recvTCP, msg);
pause(1); % wait for response
msg = sprintf('POST /rab/user/stream?userid=%d&contractid=%d HTTP/1.0\nConnection: Keep-Alive\n\n',userid,contractid);
fprintf(recvTCP,msg);
pause(1); % wait for response

if(negotiationStatus == 1)
    set(hObject,'String','Sold!');
    set(hObject,'Enable','off');
    set(handles.lifestyle,'Enable','off');
    set(handles.freqbands,'Enable','off');
    set(handles.detthreshold,'Enable','off');
    
    set(handles.streamtype,'Enable','on');
    set(handles.stream,'Enable','on');
    set(handles.streamuri,'Enable','on');
    set(handles.streamport,'Enable','on');
end

%initialize_gui(gcbf, handles, true);

function streamport_Callback(hObject, eventdata, handles)
% hObject    handle to streamport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of streamport as text
%        str2double(get(hObject,'String')) returns contents of streamport as a double
handles.mydata.streamport = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);


function streamuri_Callback(hObject, eventdata, handles)
% hObject    handle to streamuri (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of streamuri as text
%        str2double(get(hObject,'String')) returns contents of streamuri as a double
handles.mydata.streamuri = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);


% --- Executes on selection change in streamtype.
function streamtype_Callback(hObject, eventdata, handles)
% hObject    handle to streamtype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% hObject    handle to freqbands (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.mydata.bandIDx = get(hObject,'Value');
set(handles.price,'String',sprintf('$%d',handles.mydata.PRICELOOKUP(handles.mydata.bandIDx,handles.mydata.thresh)));
% Update handles structure
guidata(hObject, handles);


% --- Executes on selection change in detthreshold.
function detthreshold_Callback(hObject, eventdata, handles)
% hObject    handle to detthreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.mydata.thresh = get(hObject,'Value');
set(handles.price,'String',sprintf('$%d',handles.mydata.PRICELOOKUP(handles.mydata.bandIDx,handles.mydata.thresh)));
% Update handles structure
guidata(hObject, handles);


function serviceport_Callback(hObject, eventdata, handles)
% hObject    handle to serviceport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of serviceport as text
%        str2double(get(hObject,'String')) returns contents of serviceport as a double
handles.mydata.serviceport = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function serviceport_CreateFcn(hObject, eventdata, handles)
% hObject    handle to serviceport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function serviceuri_Callback(hObject, eventdata, handles)
% hObject    handle to serviceuri (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of serviceuri as text
%        str2double(get(hObject,'String')) returns contents of serviceuri as a double

handles.mydata.serviceuri = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function streamport_CreateFcn(hObject, eventdata, handles)
% hObject    handle to streamport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function streamuri_CreateFcn(hObject, eventdata, handles)
% hObject    handle to streamuri (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in stream.
function stream_Callback(hObject, eventdata, handles)
% hObject    handle to stream (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global recvTCP recvUDP

if(strcmp(get(hObject,'String'),'Stream'))
    recvUDP = udp('', 'LocalPort', handles.mydata.streamport); 
    recvUDP.BytesAvailableFcnMode = 'terminator';
    recvUDP.Terminator = 'LF';
    recvUDP.BytesAvailableFcn = {@recvUDP_cb};

    fopen(recvUDP);
    if(strcmp(recvUDP.status, 'open'))
        userid = 0;
        msg = sprintf('POST /rab/user/observer?userid=%d&uri=%s&port=%d HTTP/1.0\nConnection: Keep-Alive\n\n', ...
                        userid, handles.mydata.streamuri, handles.mydata.streamport);
        fprintf(recvTCP,msg);

        set(handles.streamtype,'Enable','off');
        set(handles.streamuri,'Enable','off');
        set(handles.streamport,'Enable','off');
        set(hObject,'String','Stop');
%        timerGraph = timer('TimerFcn',@timerGraph_cb, 'Period', 0.500, 'ExecutionMode','fixedRate');
    else
        fclose(recvUDP);
        delete(recvUDP);
    end
else
    fclose(recvUDP);
    delete(recvUDP);
    set(hObject,'String','Stream');
    set(hObject,'Enable','off');
    
    set(handles.negotiate,'String','Negotiate');
    set(handles.negotiate,'Enable','on');
    set(handles.lifestyle,'Enable','on');
    set(handles.freqbands,'Enable','on');
    set(handles.detthreshold,'Enable','on');
end

% --- Executes on selection change in lifestyle.
function lifestyle_Callback(hObject, eventdata, handles)
% hObject    handle to lifestyle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lifestyle contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lifestyle
handles.mydata.lifestyle = get(hObject,'Value');
% Update handles structure
if (handles.mydata.lifestyle == 1)
    set(handles.freqbands,'String',{'Up to 125Hz'});
elseif (handles.mydata.lifestyle == 2)
    set(handles.freqbands,'String',{'Up to 125Hz','Up to 500Hz','1kHz - 4kHz'});
else
    set(handles.freqbands,'String',{'Up to 125Hz','Up to 500Hz','1kHz - 4kHz','500Hz - 4kHz','Up to 4kHz'});
end
guidata(hObject, handles);

% --- Executes on selection change in freqbands.
function freqbands_Callback(hObject, eventdata, handles)
% hObject    handle to freqbands (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns freqbands contents as cell array
%        contents{get(hObject,'Value')} returns selected item from freqbands
handles.mydata.bandIDx = get(hObject,'Value');
set(handles.price,'String',sprintf('$%d',handles.mydata.PRICELOOKUP(handles.mydata.bandIDx,handles.mydata.thresh)));
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
% hObject    handle to reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(instrfind);
delete(timerfind);
initialize_gui(gcbf, handles, 1);
