% Test user interface to the RAB
%
% Long Le, longle1@illinois.edu
% University of Illinois
%

clear all; close all;
addpath('../../common');
% Clear deprecated timer or instr object
delete(timerfind);
delete(instrfind);

%% Configuration 

% Configure TCPIP
global recvTCP
recvTCP=tcpip('localhost', 8086);
recvTCP.InputBufferSize = 2^16;
recvTCP.BytesAvailableFcnMode = 'terminator';
recvTCP.Terminator = 'LF';
recvTCP.BytesAvailableFcn = @recvTCP_cb;

% Configure timer
%timerGET = timer('TimerFcn',@timerGET_cb, 'Period', 2.5, 'ExecutionMode','fixedRate');

% Configure a server. Note: tcpip server does not work with Ptolemy
%{
import java.net.ServerSocket
import java.io.*
localSocket = ServerSocket(8089);
localSocket.setSoTimeout(30000);
%}

% Configure a UDP server
% Allow receiving from any remote host without filtering.
% Default localhost is INADDR_ANY 
u = udp('', 'LocalPort', 8089); 
u.BytesAvailableFcnMode = 'terminator';
u.Terminator = 'LF';
u.BytesAvailableFcn = {@recvUDP_cb};

%% Main

fopen(recvTCP);
%start(timerGET);
fopen(u);

fprintf(recvTCP, ['POST /rab/user/contract?userid=0&contractid=0&avgErate=14&price=200 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response
fprintf(recvTCP, ['POST /rab/user/control?userid=0&enable=true HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response
fprintf(recvTCP, ['POST /rab/user/stream?userid=0&contractid=0 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response
%{
fprintf(recvTCP, ['POST /rab/user/observer?userid=0&url=http://localhost&port=8089 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response
%}
fprintf(recvTCP, ['POST /rab/user/observer?userid=0&uri=localhost&port=8089 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
pause(1); % wait for response

for k = 1:15
    %{
    try
        remoteSocket = localSocket.accept();
    catch
        break;
    end
    request = httpHandler(remoteSocket)
    %}
    pause(2);
end

fclose(u);
%localSocket.close;
%stop(timerGET);
fclose(recvTCP);
