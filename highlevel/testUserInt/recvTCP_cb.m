% HTTP response handler using TCP callback
%
% Long Le, longle1@illinois.edu
% University of Illinois
% 05/18/2014
%
% [DJ 05/27/2014]: Added hacky code to check for any server response. Used 
% in testConnGUI, execution blocks for a short time (e.g., 1 sec) before 
% checking negotiationStatus.
%
function recvTCP_cb(obj, event)

global negotiationStatus

if(negotiationStatus == 0)
    negotiationStatus = 1;
end


data = fscanf(obj);
disp(data);
end