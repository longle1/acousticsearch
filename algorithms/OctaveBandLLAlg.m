% Octave band low level algorithm
% 
% Long Le, longle1@illinois.edu
% University of Illinois
%

clear all; close all;

addpath('../common');

RECORD_FLAG = false;

%% Data recording
if RECORD_FLAG
    % Configure audio recorder
    fs = 48000;
    Nblk = 0.100*fs;
    rec = dsp.AudioRecorder('SampleRate',fs,'NumChannels',1,'QueueDuration',1.5,'SamplesPerFrame',Nblk);
    T = 1000;
    data = zeros(Nblk,T);
    disp('Start recording...')
    for k = 1:T
        data(:,k) = step(rec);
    end
    disp('Stop recording!')
    save data.mat data fs
else
    load data.mat data fs
    T = size(data, 2);
end

%% Processing parameters
BandsPerOctave = 1;
N = 6;           % Filter Order
F0 = 500;       % Center Frequency (Hz)
f = fdesign.octave(BandsPerOctave,'Class 1','N,F0',N,F0,fs);
F0 = validfrequencies(f);
Nfc = length(F0);
for i=1:Nfc,
    f.F0 = F0(i);
    %Hd(i) = design(f,'FilterStructure','df1sos');
    Hd(i) = design(f,'butter');
end

% noise tracking parameters
alpha_updown = 0.05;
slow_scale = 0.5;
noisetracker.indicator_countdown = 2;
noisetracker.floor_up = (1+alpha_updown)*ones(Nfc,1);
noisetracker.floor_up_slow = (1+slow_scale*alpha_updown)*ones(Nfc,1);
noisetracker.floor_down = (1-alpha_updown)*ones(Nfc,1);
noisetracker.floor_thresh = ones(Nfc,1);
noisetracker.det_thresh_scale = 4.0*ones(Nfc,1);
noisetracker.indicator_last = zeros(Nfc,1);

%% Main
curevt = zeros(Nfc, T);
for k = 1:T
    for j=1:Nfc
        % compute test statistic
        s = filter(Hd(j),data(:,k));
        y = norm(s);

        % noise tracking
        if(y > noisetracker.floor_thresh(j))
            noisetracker.indicator_last(j) = noisetracker.indicator_last(j)-1;
            if(noisetracker.indicator_last(j) < 0) % signal is highly probable, slow down
                noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up_slow(j);
            else % transient signal is highly probable
                noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up(j);
            end
        else % background noise is highly probable
            noisetracker.indicator_last(j) = noisetracker.indicator_countdown;
            noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_down(j);
        end
        % get labels (via declaring detection)
        if(y > noisetracker.det_thresh_scale(j)*noisetracker.floor_thresh(j))
            curevt(j,k) = 1;
        end
    end
end

figure; imagesc(curevt);
axis xy
