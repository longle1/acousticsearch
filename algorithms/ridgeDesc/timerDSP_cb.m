% Callback for signal processing
% 
% Long Le, longle1@illinois.edu
% University of Illinois
%

function timerDSP_cb(obj, event)
%tic
global rec fs idx

global fh tBuf sBuf rBuf

global ridgetracker noisetracker

Nfc = size(ridgetracker.log_prob_noise_ridge_cum_now,1);

%disp('In the callback.')
data = step(rec);
w = hamming(numel(data));
s = abs(fft(w.*data));
s = s(1:Nfc);

tmpridgetracker = ridgetracker;
tmpnoisetracker = noisetracker;
[ridgetracker, noisetracker] = ridgeDetectOnline(s, ones(Nfc,1), idx-ridgetracker.backtracklen, fs, tmpridgetracker, tmpnoisetracker);

% Detect pause-interval between tracking-interval
rNew = cell(0);
if sum(ridgetracker.stftdetectridgeback(:,2)) == 0
    for k = 1:ridgetracker.ridgect
        % Skip trivial ridges in advance
        if size(ridgetracker.ridge_desc{k},2) == 1
            continue;
        end
        lowfreq = zeros(1, size(ridgetracker.ridge_desc{k}, 2));
        highfreq = zeros(1, size(ridgetracker.ridge_desc{k}, 2));
        for l = 1:size(ridgetracker.ridge_desc{k}, 2)
            d = diff([0; ridgetracker.ridge_desc{k}(:,l); 0]);
            lowIdx = find(d == 1);
            highIdx = find(d == -1);
            lowfreq(l) = lowIdx(1)/Nfc*fs/2;
            highfreq(l) = (highIdx(end)-1)/Nfc*fs/2;
        end
        rNew{k} = [[ridgetracker.ridge_min_time(k):(ridgetracker.ridge_max_time(k)-ridgetracker.ridge_min_time(k))/(size(ridgetracker.ridge_desc{k},2)-1):ridgetracker.ridge_max_time(k)];...
            lowfreq;highfreq];
        % debugging
        %disp(rNew)
    end
    rNew(cellfun(@isempty, rNew)) = [];
    ridgetracker.ridgect = 0;
end

% Spectrogram
% DEBUG plot "live" data
tBuf = [tBuf(:,2:end) idx];
sBuf = [sBuf(:,2:end) s];
rBuf = {rBuf{2:end} rNew};
figure(fh);
imagesc(tBuf, [1:Nfc]/Nfc*fs/2, sBuf);
caxis([0,0.5]);
axis tight; axis xy;
% increment index pointer
idx = idx + 1;

winlen = size(rBuf, 2);
for k = 1:winlen
    if ~isempty(rBuf{k})
        for l = 1:numel(rBuf{k})
            hold on
            x = repmat(rBuf{k}{l}(1,:)',1,2);
            y = [rBuf{k}{l}(2,:)' rBuf{k}{l}(3,:)'];
            plot(x, y, 'color', 'y','linewidth', 2);
            hold off
        end
    end
end