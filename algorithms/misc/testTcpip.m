% Test Matlab tcpip for persistent connection
%
% Long Le
% longle1@illinois.edu
% University of Illinois
%

clear all; close all;

isLocalhost = true;

if isLocalhost
    t = tcpip('localhost', 8086);
else
    t = tcpip('www.google.com', 80);
end
t.InputBufferSize = 2^16;
t.BytesAvailableFcnMode = 'terminator';
t.Terminator = 'LF';
t.BytesAvailableFcn = @recvTCP_cb;

fopen(t);
if isLocalhost
    % This works
    %fprintf(t, ['POST /rab/lowlevel?freq=1&noise=1&sig=1&time=1 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
    %fprintf(t, ['POST /rab/lowlevel?freq=1&noise=1&sig=1&time=1 HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);
    % This doesn't work
    fprintf(t, ['POST /rab/lowlevel?freq=1&noise=1&sig=1&time=1 HTTP/1.1' 13 10]);
    fprintf(t, ['POST /rab/lowlevel?freq=1&noise=1&sig=1&time=1 HTTP/1.1' 13 10]);
else
    fprintf(t, ['HEAD / HTTP/1.1' 10 10]);
    fprintf(t, ['HEAD / HTTP/1.1' 10 10]);
end
pause(1); % wait for response
fclose(t);

