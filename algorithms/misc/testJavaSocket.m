% Test java server socket to connect with Ptolemy client
%
% Long Le, longle1@illinois.edu
% University of Illinois
%

clear all; close all;

import java.net.ServerSocket
import java.io.*
localSocket = ServerSocket(8089);
localSocket.setSoTimeout(30000);

remoteSocket = localSocket.accept();

writer = OutputStreamWriter(remoteSocket.getOutputStream());
writer.write(sprintf('HTTP/1.1 200 OK\n'));
writer.flush();
%remoteSocket.shutdownOutput; % Fix deadlock with Ptolemy remote socket

reader = BufferedReader(InputStreamReader(remoteSocket.getInputStream()));
request = [];
while (1)
    line = reader.readLine();
    if isempty(line)
        break;
    end
    request = [request char(line) 10];
end
request

remoteSocket.close;
localSocket.close;