clear all; close all;

BandsPerOctave = 1;
N = 6;           % Filter Order
F0 = 1000;       % Center Frequency (Hz)
Fs = 44100;      % Sampling Frequency (Hz)
f = fdesign.octave(BandsPerOctave,'Class 1','N,F0',N,F0,Fs);

F0 = validfrequencies(f);
Nfc = length(F0);
for j=1:Nfc,
    f.F0 = F0(j);
    Hd(j) = design(f,'butter');
end

hfvt = fvtool(Hd,'FrequencyScale','log','color','white');
axis([0.01 24 -90 5])
title('Octave-Band Filter Bank')