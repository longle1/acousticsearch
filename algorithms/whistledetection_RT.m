% whistledetection_RT.m
%
% streaming example of robust whistle detection algorithm. buffer overruns
% occur when plotting, even if only plotting detection statistic, which is
% downsampled by a factor of Nblk compared to plotting the raw data.
%
% David Jun
% University of Illinois
% 05/26/2014

clear all;
close all;

if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
    fs = 22050;
    Nblk = 1024;
    % run for 60 seconds
    Nblks = floor(60*fs / Nblk);
    rec = dsp.AudioRecorder('SampleRate',fs,'NumChannels',1,'SamplesPerFrame',Nblk);
else
    [rec, fs] = audioread('../data/whistlenoisedata.wav');
    rec = resample(rec,8000,fs);
    fs = 8000;
    Nblk = 128;
    Nblks = floor(length(rec) / Nblk);
end

win = hann(Nblk);

% noise tracking
Nfc = Nblk;
alpha_updown = 0.05;
slow_scale = 0.5;
indicator_countdown = 2;
floor_up = (1+alpha_updown)*ones(Nfc,1);
floor_up_slow = (1+slow_scale*alpha_updown)*ones(Nfc,1);
floor_down = (1-alpha_updown)*ones(Nfc,1);
floor_thresh = ones(Nfc,1);
det_thresh_scale = 100.0*ones(Nfc,1);
indicator_last = zeros(Nfc,1);
floor_track = zeros(Nfc,Nblks);
wiener_scale = 0.01;
wiener_min = 0.1;

binmin = floor(500/fs*2*Nfc)+1;
binmax = floor(4000/fs*2*Nfc);

y = zeros(Nfc,Nblks);
evt = zeros(Nfc,Nblks);
yN = zeros(Nfc,Nblks);
wienerweights = zeros(Nfc,Nblks);
%%

% bandwidth around max frequency (assumed to be the whistle)
whistleBW = 8;

% DEBUG
%fh = figure;
figure();
bufX = zeros(100*Nblk,1);
timebufX = (-100*Nblk+1:0)';
subplot(2,1,1);
phX = plot(timebufX,bufX);
set(phX,'XDataSource','timebufX');
set(phX,'YDataSource','bufX');

bufZ = zeros(100,1);
timebufZ = (-99:0)';
subplot(2,1,2);
phZ = plot(timebufZ,bufZ);
set(phZ,'XDataSource','timebufZ');
set(phZ,'YDataSource','bufZ');

z = zeros(Nblks,1);
for i=1:Nblks
    if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
        x = step(rec);
    else
        x = rec((i-1)*Nblk+1:i*Nblk);
    end
    % compute test statistic
    xfmag = abs(fft(x.*win,2*Nfc));
    y(:,i) = xfmag(1:Nfc);

    for j=1:Nfc
        % noise tracking
        if(y(j,i) > floor_thresh(j))
            indicator_last(j) = indicator_last(j)-1;
            if(indicator_last(j) < 0) % signal is highly probable, slow down
                floor_thresh(j) = floor_thresh(j)*floor_up_slow(j);
            else % transient signal is highly probable
                floor_thresh(j) = floor_thresh(j)*floor_up(j);
            end
        else % background noise is highly probable
            indicator_last(j) = indicator_countdown;
            floor_thresh(j) = floor_thresh(j)*floor_down(j);
        end
        
        % get labels (via declaring detection)
        if(y(j,i) > det_thresh_scale(j)*floor_thresh(j))
            evt(j,i) = 1;
            % this is where we would POST an event to high-level search
            % include: 
            %   freq band       j
            %   timestamp       i
            %   sig power       y(j,i)
            %   noise power     floor_thresh(j)
            %   thresh scale    det_thresh_scale(j)
        end
    end
    floor_track(:,i) = floor_thresh;
    
    % denoising. detection operates on wiener-filtered data, which 
    % normalizes for fluctuations in background noise (but does not reduce 
    % interference)
    wienerweights(:,i) = max(min(1,wiener_scale*y(:,i)./floor_thresh),wiener_min);
    yN(:,i) = y(:,i) .* wienerweights(:,i);
    
    
    % whistle detection: assumes signal is of a single unknown frequency, 
    % and is designed for detection in interference
    frame = yN(:,i);    
    [val, idx] = max(frame);
    if(idx>=binmin && idx<=binmax)
        ids = binmin:binmax;
        st = idx-whistleBW/2;
        en = idx+whistleBW/2;
        z(i) = val / mean(frame(ids(~(ids>=st&ids<=en))));
    else
        z(i) = 0;
    end

    % DEBUG: plot raw signal
%     bufX = [bufX(Nblk+1:end);x];
%     timebufX = [timebufX(Nblk+1:end); timebufX(end)+(1:Nblk)'];
%     subplot(2,1,1);axis([timebufX(1), timebufX(end), -1 1]);
%     
%     % DEBUG: plot detection statistic
%     bufZ = [bufZ(2:end);z(i)];
%     timebufZ = [timebufZ(2:end); timebufZ(end)+1];
%     subplot(2,1,2);axis([timebufZ(1), timebufZ(end), 0 12000]);

%    refreshdata(fh);
%    drawnow;


end

if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
    release(rec);
end



