% Test online ridge detector

clear all; close all
addpath('../../voicebox/');
addpath('../common/');

%% Parameters
%[x, fs] = wavread('../../ridgeDet/GCWarbler40561.wav');
%[x, fs] = wavread('../../acousticsearch/data/whistlecommand/9-3.wav');
[x, fs] = wavread('../../matlab-realtime-audioprocessing/TIDIGIT_adults_crop/train/2/cb.wav');

Nblk = 256;
Nfc = 256;
w = hann(Nblk*2)';

noisetracker.alpha_updown = 0.01;
noisetracker.slow_scale = 0.1;
noisetracker.indicator_countdown = 10;
noisetracker.floor_up = (1+noisetracker.alpha_updown)*ones(Nfc,1);
noisetracker.floor_up_slow = (1+noisetracker.slow_scale*noisetracker.alpha_updown)*ones(Nfc,1);
noisetracker.floor_down = (1-noisetracker.alpha_updown)*ones(Nfc,1);
noisetracker.floor_thresh = 0.1*ones(Nfc,1);
noisetracker.det_thresh_scale = 4.0*ones(Nfc,1);
noisetracker.indicator_last = zeros(Nfc,1);

% Ridge tracking parameter

% Parameters
ridgetracker.alpha_ridge = exp(-Nblk/(0.1*fs));
ridgetracker.f_ridge_down_max = ceil(20000*Nblk*Nfc*2/(fs*fs)); % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
ridgetracker.f_ridge_up_max = ridgetracker.f_ridge_down_max;
ridgetracker.f_ridge_down = zeros(Nfc,1);
ridgetracker.f_ridge_up = zeros(Nfc,1);
for ii=1:Nfc,
   ridgetracker.f_ridge_down(ii) = max(1,ii-ridgetracker.f_ridge_down_max);
   ridgetracker.f_ridge_up(ii) = min(Nfc,ii+ridgetracker.f_ridge_up_max);
end
ridgetracker.ridge_offset_penalty_max = 0.05;
ridgetracker.ridge_offset_penalty = ridgetracker.ridge_offset_penalty_max/max(ridgetracker.f_ridge_up_max,ridgetracker.f_ridge_down_max);
ridgetracker.log_prob_noise_min_ridge = -6.0;
ridgetracker.detect_ridge_backtrack_thresh = -4.0;
ridgetracker.backtracklen = ceil(0.05*fs/Nblk); % enter the multiplier as the approximate track length in sec
% States
ridgetracker.log_prob_noise_ridge_cum_now = zeros(Nfc,1);
ridgetracker.specridge_link_back = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.speclog_prob_noise_ridge = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.stftdetectridgeback = zeros(Nfc,2);
ridgetracker.ridge_min_time = zeros(1,0);
ridgetracker.ridge_max_time = zeros(1,0);
ridgetracker.ridge_min_freq = zeros(1,0);
ridgetracker.ridge_max_freq = zeros(1,0);
ridgetracker.ridge_loglik = zeros(1,0);
ridgetracker.ridge_desc = cell(1,0);
ridgetracker.ridgect = 0;

%% Main
data = enframe(x,Nblk*2, Nblk);
spec = zeros(Nfc, size(data,1));
spec_noise_floor = zeros(Nfc, size(data,1));
specridge_link_back = zeros(Nfc, size(data,1));
speclog_prob_noise_ridge = zeros(Nfc, size(data,1));
specbacktrackcumsum = zeros(Nfc, size(data,1));
specbacktracktouchedflags = zeros(Nfc, size(data,1));
stftdetectridgeback = zeros(Nfc, size(data,1));
for k = 1:size(data,1)
    s = abs(fft(w.*data(k,:)));
    s = s(1:Nfc);
    spec(:,k) = s;

    %{
    offset_ridge = zeros(Nfc,1);
    ridge_link_back = zeros(Nfc,1);
    log_prob_noise_ridge = zeros(Nfc,1);
    log_prob_noise_ridge_cum_last = ridgetracker.log_prob_noise_ridge_cum_now;
    for j=1:Nfc
        % noise tracking
        if(s(j) > noisetracker.floor_thresh(j))
            noisetracker.indicator_last(j) = noisetracker.indicator_last(j)-1;
            if(noisetracker.indicator_last(j) < 0) % signal is highly probable, slow down
                noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up_slow(j);
            else % transient signal is highly probable
                noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up(j);
            end
        else % background noise is highly probable
            noisetracker.indicator_last(j) = noisetracker.indicator_countdown;
            noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_down(j);
        end
        
        % debugging
        spec_noise_floor(:,k) = noisetracker.floor_thresh;

        %  TIME-VARYING SPECTRAL RIDGE TRACKING
        log_prob_noise_ridge(j) = max(ridgetracker.log_prob_noise_min_ridge,-s(j)^2/noisetracker.floor_thresh(j));
        [~, offset_ridge(j)] = ...
            min((1 - ridgetracker.ridge_offset_penalty*abs(j - [ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)]')).*...
            log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)));
        
        ridgetracker.log_prob_noise_ridge_cum_now(j) = ridgetracker.alpha_ridge*log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j)+offset_ridge(j)-1) + ...
              (1-ridgetracker.alpha_ridge)*log_prob_noise_ridge(j);
        ridge_link_back(j) = ridgetracker.f_ridge_down(j)+offset_ridge(j)-1;  % pointers to the previous ridge frequency
    end
    ridgetracker.specridge_link_back = [ridgetracker.specridge_link_back(:,2:end) ridge_link_back];
    ridgetracker.speclog_prob_noise_ridge = [ridgetracker.speclog_prob_noise_ridge(:,2:end) log_prob_noise_ridge];
    
    % debugging
    speclog_prob_noise_ridge(:,k) = log_prob_noise_ridge;
    specridge_link_back(:,k) = ridge_link_back;
    
    %
    % Backward track detection
    %
    % track back backtracklen time blocks
    backtrackcumsum = log_prob_noise_ridge;
    backtracktouchedflags = ones(Nfc,1);
    for i5=ridgetracker.backtracklen+1:-1:2,
      backtrackcumsumnew = zeros(Nfc,1);
      backtracktouchedflagsnew = zeros(Nfc,1);
      % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
      for iii=1:Nfc
         if ( backtracktouchedflags(iii) == 1),
            if ridgetracker.specridge_link_back(iii,i5) == 0
                continue;
            end
            backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)) = ...
                min(backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)),...
                backtrackcumsum(iii)+ridgetracker.speclog_prob_noise_ridge(ridgetracker.specridge_link_back(iii,i5),i5-1));
            backtracktouchedflagsnew(ridgetracker.specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
         end
      end
      backtrackcumsum = backtrackcumsumnew;
      backtracktouchedflags = backtracktouchedflagsnew;
    end
    backtrackcumsum = (1.0/(ridgetracker.backtracklen+1))*backtrackcumsum;

    % debugging
    if (k > ridgetracker.backtracklen)
        specbacktrackcumsum(:,k-ridgetracker.backtracklen) = backtrackcumsum;
        specbacktracktouchedflags(:,k-ridgetracker.backtracklen) = backtracktouchedflags;
    end

    %
    % find t-f locations detected as part of ridge
    %
    detected_ridge_back = zeros(Nfc,1);
    for iii=1:Nfc
      if ( backtrackcumsum(iii) < ridgetracker.detect_ridge_backtrack_thresh ),
         if ridgetracker.specridge_link_back(iii,1) == 0
             continue;
         end
         % pickup from the previous ridge_index
         ridge_index = ridgetracker.stftdetectridgeback(ridgetracker.specridge_link_back(iii,1),2);
         if ( ridge_index == 0 ),
            ridgetracker.ridgect = ridgetracker.ridgect + 1;
            ridge_index = ridgetracker.ridgect;
            ridgetracker.ridge_min_time(ridge_index) = k-ridgetracker.backtracklen;
            ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
            ridgetracker.ridge_min_freq(ridge_index) = iii/Nfc*fs/2;
            ridgetracker.ridge_max_freq(ridge_index) = iii/Nfc*fs/2;
            ridgetracker.ridge_loglik(ridge_index) = backtrackcumsum(iii);
            ridgetracker.ridge_desc{ridge_index} = [];
         else
            ridgetracker.ridge_max_time(ridge_index) = k-ridgetracker.backtracklen;
            ridgetracker.ridge_min_freq(ridge_index) = min(iii/Nfc*fs/2,ridgetracker.ridge_min_freq(ridge_index));
            ridgetracker.ridge_max_freq(ridge_index) = max(iii/Nfc*fs/2,ridgetracker.ridge_max_freq(ridge_index));
            ridgetracker.ridge_loglik(ridge_index) = ridgetracker.ridge_loglik(ridge_index)+backtrackcumsum(iii);
         end
         detected_ridge_back(iii) = ridge_index;
      end
    end
    ridgetracker.stftdetectridgeback = [ridgetracker.stftdetectridgeback(:,2) detected_ridge_back];
    ridge_index_set = unique(detected_ridge_back);
    for l = 1:numel(ridge_index_set)
        if (ridge_index_set(l) == 0)
            continue
        end
        ridgetracker.ridge_desc{ridge_index_set(l)} = [ridgetracker.ridge_desc{ridge_index_set(l)} detected_ridge_back==ridge_index_set(l)];
    end
    
    % debugging
    if (k > ridgetracker.backtracklen)
        stftdetectridgeback(:,k-ridgetracker.backtracklen) = detected_ridge_back;
    end
    %}
    [ridgetracker, noisetracker] = ridgeDetectOnline(s, ones(1,Nfc), k-ridgetracker.backtracklen, fs, ridgetracker, noisetracker, Inf);
end

% Remove trivial ridges
idxT = ridgetracker.ridge_max_time > ridgetracker.ridge_min_time;
idxF = ridgetracker.ridge_max_freq > ridgetracker.ridge_min_freq;
idx = idxT & idxF;
ridgetracker.ridgect = sum(idx);
ridgetracker.ridge_min_time(~idx) = [];
ridgetracker.ridge_max_time(~idx) = [];
ridgetracker.ridge_min_freq(~idx) = [];
ridgetracker.ridge_max_freq(~idx) = [];
ridgetracker.ridge_loglik(~idx) = [];
ridgetracker.ridge_desc(~idx) = [];

% Highlight time-frequency blocks
figure; hold on;
imagesc([1:size(data,1)]*Nblk/fs, [1:Nfc]/Nfc*fs/2, log(spec))
for k = 1:ridgetracker.ridgect
    lowfreq = zeros(1, size(ridgetracker.ridge_desc{k}, 2));
    highfreq = zeros(1, size(ridgetracker.ridge_desc{k}, 2));
    for l = 1:size(ridgetracker.ridge_desc{k}, 2)
        d = diff([0; ridgetracker.ridge_desc{k}(:,l); 0]);
        lowIdx = find(d == 1);
        highIdx = find(d == -1);
        lowfreq(l) = lowIdx(1)/Nfc*fs/2;
        highfreq(l) = (highIdx(end)-1)/Nfc*fs/2;
    end
    plot([ridgetracker.ridge_min_time(k):(ridgetracker.ridge_max_time(k)-ridgetracker.ridge_min_time(k))/(size(ridgetracker.ridge_desc{k},2)-1):ridgetracker.ridge_max_time(k)]*Nblk/fs,...
        highfreq, 'k', 'linewidth', 2)
    plot([ridgetracker.ridge_min_time(k):(ridgetracker.ridge_max_time(k)-ridgetracker.ridge_min_time(k))/(size(ridgetracker.ridge_desc{k},2)-1):ridgetracker.ridge_max_time(k)]*Nblk/fs,...
        lowfreq, 'k', 'linewidth', 2)
end
axis('tight')
title(sprintf('Ridge counts: %d', ridgetracker.ridgect), 'fontsize', 15);
set(gca, 'fontsize', 15)
xlabel('Time (s)', 'fontsize', 15); ylabel('Frequency (Hz)', 'fontsize', 15)
