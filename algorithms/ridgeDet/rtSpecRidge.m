% Realtime spectrogram and ridge detection
%
% Long Le
% University of Illinois
%

clear all; close all;

%addpath('../../../voicebox/');
addpath('../../common/');

% Clear deprecated timer
delete(timerfind);

%% Parameters
global fs idx
fs = 8000;
Nblk = 256;
Nfc = 256;
idx = 1;
% Instruments and timers
global rec
rec = dsp.AudioRecorder('SampleRate',fs,'NumChannels',1,'BufferSizeSource','Property','BufferSize',8192,...
    'QueueDuration',2,'SamplesPerFrame',Nblk);
timerDSP = timer('TimerFcn',@timerDSP_cb, 'Period', Nblk/fs, 'ExecutionMode','fixedRate');

winlen = 150;
global tBuf sBuf rBuf fh
tBuf = zeros(1, winlen);
sBuf = zeros(Nfc, winlen);
rBuf = cell(1, winlen);
for k = 1:winlen
    rBuf{k} = zeros(0,4);
end
fh = figure;

global noisetracker
alpha_updown = 0.01;
slow_scale = 0.1;
noisetracker.indicator_countdown = 10;
noisetracker.floor_up = (1+alpha_updown)*ones(Nfc,1);
noisetracker.floor_up_slow = (1+slow_scale*alpha_updown)*ones(Nfc,1);
noisetracker.floor_down = (1-alpha_updown)*ones(Nfc,1);
noisetracker.floor_thresh = 0.1*ones(Nfc,1);
noisetracker.det_thresh_scale = 4.0*ones(Nfc,1);
noisetracker.indicator_last = zeros(Nfc,1);

% Ridge tracking parameter
global ridgetracker
ridgetracker.alpha_ridge = exp(-Nblk/(0.1*fs));
ridgetracker.f_ridge_down_max = ceil(20000*Nblk*Nfc*2/(fs*fs)); % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
ridgetracker.f_ridge_up_max = ridgetracker.f_ridge_down_max;
ridgetracker.f_ridge_down = zeros(Nfc,1);
ridgetracker.f_ridge_up = zeros(Nfc,1);
for ii=1:Nfc,
   ridgetracker.f_ridge_down(ii) = max(1,ii-ridgetracker.f_ridge_down_max);
   ridgetracker.f_ridge_up(ii) = min(Nfc,ii+ridgetracker.f_ridge_up_max);
end
ridgetracker.ridge_offset_penalty_max = 0.05;
ridgetracker.ridge_offset_penalty = ridgetracker.ridge_offset_penalty_max/max(ridgetracker.f_ridge_up_max,ridgetracker.f_ridge_down_max);
ridgetracker.log_prob_noise_min_ridge = -6.0;
ridgetracker.detect_ridge_backtrack_thresh = -4.0;
ridgetracker.backtracklen = ceil(0.05*fs/Nblk); % enter the multiplier as the approximate track length in sec
% States
ridgetracker.log_prob_noise_ridge_cum_now = zeros(Nfc,1);
ridgetracker.specridge_link_back = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.speclog_prob_noise_ridge = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.stftdetectridgeback = zeros(Nfc,2);
ridgetracker.ridge_min_time = zeros(1,0);
ridgetracker.ridge_max_time = zeros(1,0);
ridgetracker.ridge_min_freq = zeros(1,0);
ridgetracker.ridge_max_freq = zeros(1,0);
ridgetracker.ridge_loglik = zeros(1,0);
ridgetracker.ridge_desc = cell(1,0);
ridgetracker.ridgect = 0;

%% Main
start(timerDSP);

while(1)
    pause(1);
end

stop(timerDSP); delete(timerDSP); release(rec);