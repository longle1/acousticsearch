% Callback for signal processing
% 
% Long Le, longle1@illinois.edu
% University of Illinois
%

function timerDSP_cb(obj, event)
%tic
global rec fs idx

global fh tBuf sBuf rBuf

global ridgetracker noisetracker

global thresh
threshold = thresh;

Nfc = size(ridgetracker.log_prob_noise_ridge_cum_now,1);

%disp('In the callback.')
data = step(rec);
w = hamming(numel(data));
s = abs(fft(w.*data));
s = s(1:Nfc);

tmpridgetracker = ridgetracker;
tmpnoisetracker = noisetracker;
[ridgetracker, noisetracker] = ridgeDetectOnline(s, ones(Nfc,1), idx-ridgetracker.backtracklen, fs, tmpridgetracker, tmpnoisetracker);

% Detect pause-interval between tracking-interval
rNew = zeros(0,5);
if sum(ridgetracker.stftdetectridgeback(:,2)) == 0
    for k = 1:ridgetracker.ridgect
        rNew(k,:) = [ridgetracker.ridge_min_time(k) ridgetracker.ridge_min_freq(k) ...
            ridgetracker.ridge_max_time(k)-ridgetracker.ridge_min_time(k) ridgetracker.ridge_max_freq(k)-ridgetracker.ridge_min_freq(k) 0];

        pkt.time = ridgetracker.ridge_min_time(k);
        pkt.duration = ridgetracker.ridge_max_time(k) - ridgetracker.ridge_min_time(k);
        pkt.freq = (ridgetracker.ridge_max_freq(k) + ridgetracker.ridge_min_freq(k))/2;
        pkt.bw = ridgetracker.ridge_max_freq(k) - ridgetracker.ridge_min_freq(k);
        pkt.loglik = ridgetracker.ridge_loglik(k);

        if pkt.loglik < threshold && pkt.freq > 800 && pkt.freq < 1800
            display('Whistle Detected!');
            rNew(k,5) = 1;
        end
        % debugging
        %disp(rNew)
    end
    rNew(rNew(:,3) == 0 | rNew(:,4) == 0,:) = [];
    ridgetracker.ridgect = 0;
end

% Spectrogram
% DEBUG plot "live" data
tBuf = [tBuf(:,2:end) idx];
sBuf = [sBuf(:,2:end) s];
rBuf = {rBuf{2:end} rNew};
figure(fh);
imagesc(tBuf, [1:Nfc]/Nfc*fs/2, sBuf);
caxis([0,1]);
axis tight; axis xy;
% increment index pointer
idx = idx + 1;

winlen = size(rBuf, 2);
for k = 1:winlen
    if ~isempty(rBuf{k})
        for l = 1:size(rBuf{k},1)
            if rBuf{k}(l,5) == 1
                rectangle('position', rBuf{k}(l,1:4), 'edgecolor', 'r', 'linewidth', 2);
            else
                rectangle('position', rBuf{k}(l,1:4), 'edgecolor', 'y', 'linewidth', 2);
            end
        end
    end
end