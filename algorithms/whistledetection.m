function [z, floor_thresh, indicator_last] = whistledetection(x, fs, Nblk, floor_thresh,indicator_last)
% 
z = 0.0;

Nfc = Nblk;
alpha_updown = 0.05;
slow_scale = 0.5;
indicator_countdown = 2;
floor_up_slow = (1+slow_scale*alpha_updown)*ones(Nfc,1);
floor_up = (1+alpha_updown)*ones(Nfc,1);
floor_down = (1-alpha_updown)*ones(Nfc,1);
%det_thresh_scale = 100.0*ones(Nfc,1);

if(~exist('floor_thresh','var') || isempty(floor_thresh))
    floor_thresh = zeros(Nfc,1);
    indicator_last = zeros(Nfc,1);
end

wiener_scale = 0.01;
wiener_min = 0.1;

binmin = floor(500/fs*2*Nfc)+1;
binmax = floor(3500/fs*2*Nfc);

win = hann(Nblk);

% compute test statistic
xfmag = abs(fft(x.*win,2*Nfc));
y = xfmag(1:Nfc);

for j=1:Nfc
    % noise tracking
    if(y(j) > floor_thresh(j))
        indicator_last(j) = indicator_last(j)-1;
        if(indicator_last(j) < 0) % signal is highly probable, slow down
            floor_thresh(j) = floor_thresh(j)*floor_up_slow(j);
        else % transient signal is highly probable
            floor_thresh(j) = floor_thresh(j)*floor_up(j);
        end
    else % background noise is highly probable
        indicator_last(j) = indicator_countdown;
        floor_thresh(j) = floor_thresh(j)*floor_down(j);
    end

%     % get labels (via declaring detection)
%     if(y(j) > det_thresh_scale(j)*floor_thresh(j))
%         evt(j) = 1;
%         % this is where we would POST an event to high-level search
%         % include: 
%         %   freq band       j
%         %   timestamp       i
%         %   sig power       y(j,i)
%         %   noise power     floor_thresh(j)
%         %   thresh scale    det_thresh_scale(j)
end

% denoising
wienerwts = max(min(1,wiener_scale*y./floor_thresh),wiener_min);
frame = y .* wienerwts;

% bandwidth around max frequency (assumed to be the whistle)
whistleBW = 20;

% whistle detection
[val, idx] = max(frame);
if(idx>=binmin && idx<=binmax)
    ids = binmin:binmax;
    st = idx-whistleBW/2;
    en = idx+whistleBW/2;
    z = val / mean(frame(ids(~(ids>=st&ids<=en))));
else
    z = 0;
end


