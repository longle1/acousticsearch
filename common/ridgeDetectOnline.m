function [ridgetracker, noisetracker] = ridgeDetectOnline(s, bandInd, delayTime, fs, ridgetracker, noisetracker, maxNumRidge)
% Online time-frequency ridge detector using on Doug algorithm.
%[ridgetracker, noisetracker] = ridgeDetectOnline(s, bandInd, delayTime, fs, ridgetracker, noisetracker);
%
% Long Le
% University of Illinois
%
if(~isstruct(ridgetracker) || ~isstruct(noisetracker))
    perror('Invalid argument')
end
if nargin <= 6
    maxNumRidge = 10;
end

Nfc = numel(ridgetracker.log_prob_noise_ridge_cum_now);

offset_ridge = zeros(Nfc,1);
ridge_link_back = zeros(Nfc,1);
log_prob_noise_ridge = zeros(Nfc,1);
log_prob_noise_ridge_cum_last = ridgetracker.log_prob_noise_ridge_cum_now;
for j=1:Nfc
    if (bandInd(j) == 0)
        continue;
    end
    % noise tracking
    if(s(j) > noisetracker.floor_thresh(j))
        noisetracker.indicator_last(j) = noisetracker.indicator_last(j)-1;
        if(noisetracker.indicator_last(j) < 0) % signal is highly probable, slow down
            noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up_slow(j);
        else % transient signal is highly probable
            noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up(j);
        end
    else % background noise is highly probable
        noisetracker.indicator_last(j) = noisetracker.indicator_countdown;
        noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_down(j);
    end

    %  TIME-VARYING SPECTRAL RIDGE TRACKING
    log_prob_noise_ridge(j) = max(ridgetracker.log_prob_noise_min_ridge,-s(j)^2/noisetracker.floor_thresh(j));
    [~, offset_ridge(j)] = ...
        min((1 - ridgetracker.ridge_offset_penalty*abs(j - [ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)]')).*...
        log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j):ridgetracker.f_ridge_up(j)));

    ridgetracker.log_prob_noise_ridge_cum_now(j) = ridgetracker.alpha_ridge*log_prob_noise_ridge_cum_last(ridgetracker.f_ridge_down(j)+offset_ridge(j)-1) + ...
          (1-ridgetracker.alpha_ridge)*log_prob_noise_ridge(j);
    ridge_link_back(j) = ridgetracker.f_ridge_down(j)+offset_ridge(j)-1;  % pointers to the previous ridge frequency
end
ridgetracker.specridge_link_back = [ridgetracker.specridge_link_back(:,2:end) ridge_link_back];
ridgetracker.speclog_prob_noise_ridge = [ridgetracker.speclog_prob_noise_ridge(:,2:end) log_prob_noise_ridge];

%
% Backward track detection
%
% track back backtracklen time blocks
backtrackcumsum = log_prob_noise_ridge;
backtracktouchedflags = ones(Nfc,1);
for i5=ridgetracker.backtracklen+1:-1:2,
  backtrackcumsumnew = zeros(Nfc,1);
  backtracktouchedflagsnew = zeros(Nfc,1);
  % Follow pointer back from each frequency to ridge-frequency at previous time and update backward cumsum of detection metric
  for iii=1:Nfc
     if (bandInd(iii) == 0)
        continue;
     end
     if ( backtracktouchedflags(iii) == 1),
        if ridgetracker.specridge_link_back(iii,i5) == 0
            continue;
        end
        backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)) = ...
            min(backtrackcumsumnew(ridgetracker.specridge_link_back(iii,i5)),...
            backtrackcumsum(iii)+ridgetracker.speclog_prob_noise_ridge(ridgetracker.specridge_link_back(iii,i5),i5-1));
        backtracktouchedflagsnew(ridgetracker.specridge_link_back(iii,i5)) = 1;  % indicate which frequencies were part of a backtracked ridge
     end
  end
  backtrackcumsum = backtrackcumsumnew;
  backtracktouchedflags = backtracktouchedflagsnew;
end
backtrackcumsum = (1.0/(ridgetracker.backtracklen+1))*backtrackcumsum; % normalized over all of ridgetracker.speclog_prob_noise_ridge(thus including log_prob_noise_ridge)

%
% find t-f locations detected as part of ridge
%
detected_ridge_back = zeros(Nfc,1);
for iii=1:Nfc
  if (bandInd(iii) == 0)
     continue;
  end
  if ( backtrackcumsum(iii) < ridgetracker.detect_ridge_backtrack_thresh ),
     if ridgetracker.specridge_link_back(iii,1) == 0
         continue;
     end
     % pickup from the previous ridge_index
     ridge_index = ridgetracker.stftdetectridgeback(ridgetracker.specridge_link_back(iii,1),2);
     if ( ridge_index == 0 ),
        if (ridgetracker.ridgect > maxNumRidge)
            continue;
        end
        ridgetracker.ridgect = ridgetracker.ridgect + 1;
        ridge_index = ridgetracker.ridgect;
        ridgetracker.ridge_min_time(ridge_index) = delayTime;
        ridgetracker.ridge_max_time(ridge_index) = delayTime;
        ridgetracker.ridge_min_freq(ridge_index) = iii/Nfc*fs/2;
        ridgetracker.ridge_max_freq(ridge_index) = iii/Nfc*fs/2;
        ridgetracker.ridge_loglik(ridge_index) = backtrackcumsum(iii);
        ridgetracker.ridge_desc{ridge_index} = [];
     else
        ridgetracker.ridge_max_time(ridge_index) = delayTime;
        ridgetracker.ridge_min_freq(ridge_index) = min(iii/Nfc*fs/2,ridgetracker.ridge_min_freq(ridge_index));
        ridgetracker.ridge_max_freq(ridge_index) = max(iii/Nfc*fs/2,ridgetracker.ridge_max_freq(ridge_index));
        ridgetracker.ridge_loglik(ridge_index) = ridgetracker.ridge_loglik(ridge_index)+backtrackcumsum(iii);
     end
     detected_ridge_back(iii) = ridge_index;
  end
end
ridgetracker.stftdetectridgeback = [ridgetracker.stftdetectridgeback(:,2) detected_ridge_back];

% Extract desriptors
ridge_index_set = unique(detected_ridge_back);
for l = 1:numel(ridge_index_set)
    if (ridge_index_set(l) == 0)
        continue
    end
    ridgetracker.ridge_desc{ridge_index_set(l)} = [ridgetracker.ridge_desc{ridge_index_set(l)} detected_ridge_back==ridge_index_set(l)];
end