function request = httpHandler(remoteSocket)
% A simple subroutine to handle Http request
% Input
%   remoteSocket - returned socket from a call to accept()
% Output
%   request - the http request
%
% Long Le, longle1@illinois.edu
% University of Illinois
%
import java.net.ServerSocket
import java.io.*

writer = OutputStreamWriter(remoteSocket.getOutputStream());
writer.write(sprintf('HTTP/1.1 200 OK\n'));
writer.flush();
remoteSocket.shutdownOutput; % Fix deadlock with Ptolemy remote socket

reader = BufferedReader(InputStreamReader(remoteSocket.getInputStream()));
request = [];
while (1)
    line = reader.readLine();
    if isempty(line)
        break;
    end
    request = [request char(line) 10];
end

remoteSocket.close;

end