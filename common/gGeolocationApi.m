% [rawresp, resp] = gGeolocationApi(key)
% Wrapper for google geolocation api
%
% key is google API key
%
% rawresp is the raw string response
% resp is the response in json-converted/struct format
%
% Long Le
% University of Illinois
% longle1@illinois.edu
%
function [rawresp, resp] = gGeolocationApi(key)

params = {'key', key};
queryString = http_paramsToString(params);
header = http_createHeader('Content-Type', 'application/json');
% macAddr is the mac address. Can be obtained using the following command 
% for window version
% [status,result] = dos('getmac');
% macAddr = result(160:176);
rawresp = urlread2(['https://www.googleapis.com/geolocation/v1/geolocate?' queryString], 'POST', [], header, 'READ_TIMEOUT', 10000);
resp = loadjson(rawresp);

