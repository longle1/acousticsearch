function [score, p, q, C] = dpscore(x, y)
% Compute DP score between two time series with different lengths
%
% Input:
%   x, y - two time series
% Output:
%   score - min DP cost
%   p, q - shortest path
%   C - path cost matrix
%
% Long Le
% University of Illinois
%

distmat = dist2(x(:), y(:));
[p,q,C] = dpfast(distmat);
score = C(end, end);

end