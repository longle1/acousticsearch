% timerDSP_cb.m
%
% "acquires" a block of audio data. this callback is controlled by a timer,
% which is operating in real time, but can be sped up to run through data
% faster.
%
% low-level processing and event generation/construction happens here.
%
% David Jun, Long Le
% University of Illinois
% 05/25/2014

function timerDSP_cb(obj, event)
%tic
global idx Nblk rec fs

global figH tBuf sBuf rBuf

global ridgetracker noisetracker pktQ bnd thresh Nfc

threshold = thresh;
band = bnd;
bandInd = zeros(1,Nfc); % Band indicator
for k = 1:length(band)
    if any(band == 0)
        bandInd(1:floor(250/fs*Nfc*2)) = 1;
    end
    if any(band == 1)
        bandInd(floor(250/fs*Nfc*2)+1:floor(1000/fs*Nfc*2)) = 1;
    end
    if any(band == 2)
        bandInd(floor(1000/fs*Nfc*2)+1:floor(2000/fs*Nfc*2)) = 1;
    end
    if any(band == 3)
        bandInd(floor(2000/fs*Nfc*2)+1:floor(8000/fs*Nfc*2)) = 1;
    end
end
%thresh = noisetracker.det_thresh_scale;

%{
% DEBUG plot "live" data
tBuf = [tBuf(Nblk+1:end); (idx:idx+Nblk-1)'];
sBuf = [sBuf(Nblk+1:end); data];
figure(figH);
%subplot(2,1,1);
plot(tBuf, sBuf);
axis([tBuf(1), tBuf(end), -0.1 0.1]);
% increment index pointer
idx = idx + Nblk;
%}

% LOW LEVEL processing
%curevt = zeros(1, Nfc);

%disp('In the callback.')
data = step(rec);
w = hamming(numel(data));
s = abs(fft(w.*data));
s = s(1:Nfc);

tmpridgetracker = ridgetracker;
tmpnoisetracker = noisetracker;
[ridgetracker, noisetracker] = ridgeDetectOnline(s, bandInd, idx-ridgetracker.backtracklen, fs, tmpridgetracker, tmpnoisetracker);

% Detect pause-interval between tracking-interval
rNew = cell(0);
if sum(ridgetracker.stftdetectridgeback(:,2)) == 0
    for k = 1:ridgetracker.ridgect
        if size(ridgetracker.ridge_desc{k},2) == 1
            continue;
        end
        lowfreq = zeros(1, size(ridgetracker.ridge_desc{k}, 2));
        highfreq = zeros(1, size(ridgetracker.ridge_desc{k}, 2));
        for l = 1:size(ridgetracker.ridge_desc{k}, 2)
            d = diff([0; ridgetracker.ridge_desc{k}(:,l); 0]);
            lowIdx = find(d == 1);
            highIdx = find(d == -1);
            lowfreq(l) = lowIdx(1)/Nfc*fs/2;
            highfreq(l) = (highIdx(end)-1)/Nfc*fs/2;
        end
        rNew{k} = [[ridgetracker.ridge_min_time(k):(ridgetracker.ridge_max_time(k)-ridgetracker.ridge_min_time(k))/(size(ridgetracker.ridge_desc{k},2)-1):ridgetracker.ridge_max_time(k)];...
            lowfreq;highfreq];

        pkt.time = ridgetracker.ridge_min_time(k);
        pkt.duration = str2double(datestr(ridgetracker.ridge_max_time(k) - ridgetracker.ridge_min_time(k), 'SSFFF'));
        pkt.freq = (ridgetracker.ridge_max_freq(k) + ridgetracker.ridge_min_freq(k))/2;
        pkt.bw = ridgetracker.ridge_max_freq(k) - ridgetracker.ridge_min_freq(k);
        pkt.loglik = ridgetracker.ridge_loglik(k);

        if pkt.loglik < threshold
            pktQ.push(pkt);
        end
    end
    rNew(cellfun(@isempty, rNew)) = [];
    ridgetracker.ridgect = 0;
end

% Spectrogram
% DEBUG plot "live" data
tBuf = [tBuf(:,2:end) idx];
sBuf = [sBuf(:,2:end) s];
rBuf = {rBuf{2:end} rNew};
figure(figH);
imagesc(tBuf, [1:Nfc]/Nfc*fs/2, sBuf);
caxis([0,0.5]);
axis tight; axis xy;
% increment index pointer
idx = idx + 1;

winlen = size(rBuf, 2);
for k = 1:winlen
    if ~isempty(rBuf{k})
        for l = 1:size(rBuf{k},1)
            hold on
            x = repmat(rBuf{k}{l}(1,:)',1,2);
            y = [rBuf{k}{l}(2,:)' rBuf{k}{l}(3,:)'];
            plot(x, y, 'color', 'y','linewidth', 2);
            hold off
        end
    end
end
%{
% Group consecutive events into a single event
d = diff([0 curevt 0]);
dend = find(d == -1); % end of the burst + 1
dstart = find(d == 1); % start of a burst
bw = dend - dstart;

for k = 1:numel(bw)
    % this is where we would POST an event to high-level search
    % include:
    %   freq band       j
    %   timestamp       i
    %   sig power       y(j,i)
    %   noise power     floor_thresh(j)
    pkt = [];
    pkt.freq  = (dend(k)-1+dstart(k))/2/Nfc*fs/2; % center frequency
    %pkt.time  = (idx-1)/Nblk;
    c = clock;
    pkt.time  = c(4)*60^2+c(5)*60+c(6); % timestamp
    pkt.sig   = sum(s(dstart(k):dend(k)-1)); % sig power
    pkt.noise = sum(noisetracker.floor_thresh(dstart(k):dend(k)-1)); % noise power
    pkt.bw = bw(k)/Nfc*fs/2; % bandwidth

    pktQ.push(pkt);
end
%}
%{
t2Buf = [t2Buf(2:end); (idx-1)/Nblk];
s2Buf = [s2Buf(2:end,:); curevt];
% find all bands that are activated in this window
msg = sprintf('Activated bands: ');
for j=1:Nfc
    if(sum(s2Buf(:,j)) > 0)
        msg = [msg, sprintf('%i ',j)];
    end
end
subplot(2,1,2);
plot(t2Buf, s2Buf);
title(msg);
axis([t2Buf(1) t2Buf(end) -0.5 1.5]);
%}

% for i=1:10
%     subplot(11,1,i+1);
% %    plot(t2Buf,s2Buf(:,i));
%     plot(s2Buf(:,i));
% end
%     axis([t2Buf(1) t2Buf(end) -0.5 1.5]);

%toc