% HTTP response handler using TCP callback
%
% Long Le, longle1@illinois.edu
% University of Illinois
% 05/18/2014
%

function recvTCP_cb(obj, event)

global noisetracker ridgetracker bnd thresh

data = fscanf(obj);
%disp(data);
if (data(1)== '{')
    datajson = parse_json(data);
    %noisetracker.det_thresh_scale = ones(size(noisetracker.det_thresh_scale))*datajson.thresh;
    thresh = datajson.thresh;
    bnd = unique(cell2mat(datajson.bandIdx));
    fprintf(1, 'threshold: ')
    fprintf(1, '%1.4f ', thresh);
    fprintf(1, '\n');
    fprintf(1, 'bandIdx: ')
    fprintf(1, '%d ', bnd);
    fprintf(1, '\n');
else
%    fprintf(1, 'server responded.\r');
end