% timerPOST_cb.m
%
% triggered periodically to update lowlevel status. does not wait for
% a response. a response is handled by the tcpip callback.
%
% Long Le, longle1@illinois.edu
% University of Illinois
% 05/18/2014

function timerPOST_cb(obj, event)

global recvTCP

%disp('In TimerPOST')
[cap, time] = batteryReport;
msg = sprintf('POST /rab/owner/status?remain=%d&time=%d HTTP/1.0\nConnection: Keep-Alive\n\n', cap, time);
%disp(msg);
fprintf(recvTCP, msg);
fprintf(1,sprintf('POST: bat. capacity: %d, time: %d\r',cap,time));

end