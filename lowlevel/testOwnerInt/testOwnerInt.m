% Test owner interface with RAB
%
% Long Le, longle1@illinois.edu
% University of Illinois
% 

clear all; close all;
addpath('../../common');
% Clear deprecated timer or instr object
delete(timerfind);
delete(instrfind);
%% Configuration

% Configure TCPIP
global recvTCP
recvTCP = tcpip('localhost', 8086); % remote host and port
recvTCP.InputBufferSize = 2^16;
recvTCP.BytesAvailableFcnMode = 'terminator';
recvTCP.Terminator = 'LF';
recvTCP.BytesAvailableFcn = @recvTCP_cb;

% Configure timer POST
timerPOST = timer('TimerFcn',@timerPOST_cb, 'Period', 10, 'ExecutionMode','fixedRate');

% Configure timer GET
timerGET = timer('TimerFcn',@timerGET_cb, 'Period', 1, 'ExecutionMode','fixedRate');

%% Main

% Start tcp receive callback
fopen(recvTCP);
msg = sprintf(['POST /rab/owner/contract?capacity=%d&time=%d&lifetime=%d HTTP/1.0' 10 'Connection: Keep-Alive' 10 10], 10, 0, 10);
fprintf(recvTCP, msg);
% start periodic status post, need recvTCP open first
start(timerPOST);
start(timerGET);

for k = 1:15
    msg = sprintf(['POST /rab/owner/stream?sig=%1.8f&noise=%1.8f&freq=%d&time=%d HTTP/1.0' 10 'Connection: Keep-Alive' 10 10], k+0.5, 0.5, 4, 4);
    fprintf(recvTCP, msg); % icinterface/fprintf does not support runtime formatted message.
    display(msg);
    pause(2);
end

stop(timerPOST); delete(timerPOST);
stop(timerGET); delete(timerGET);
fclose(recvTCP); delete(recvTCP);
