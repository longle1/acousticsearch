% timerPOST_cb.m
%
% triggered periodically to update lowlevel status. does not wait for
% a response. a response is handled by the tcpip callback.
%
% Long Le, longle1@illinois.edu
% University of Illinois
% 05/18/2014

function timerPOST_cb(obj, event)

global recvTCP

%disp('In TimerPOST')
msg = sprintf(['POST /rab/owner/status?remain=%d&time=%d HTTP/1.0' 10 'Connection: Keep-Alive' 10 10], 5, 5);
fprintf(recvTCP, msg);

end