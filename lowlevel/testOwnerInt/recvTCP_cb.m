% HTTP response handler using TCP callback
%
% Long Le, longle1@illinois.edu
% University of Illinois
% 05/18/2014
%
function recvTCP_cb(obj, event)

data = fscanf(obj);
disp(data);
end