% timerGET_cb.m
%
% triggered periodically to request for more event data. does not wait for
% a response. a response is handled by the tcpip callback.
%
% David Jun
% University of Illinois
% 05/08/2014
%

function timerGET_cb(obj, event)

global recvTCP

fprintf(recvTCP, ['GET /rab/owner/status HTTP/1.0' 10 'Connection: Keep-Alive' 10 10]);

end