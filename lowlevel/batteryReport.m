function [cap, time, time2] = batteryReport
% Report battery capacity at the current time for window system only
% Output:
%	cap - battery capacity at the current time
%   time - time in seconds, with period of one day.
%   time2 - time in the standard format
% 
% Long Le, longle1@illinois.edu
% University of Illinois
% 

if ismac
    cap = 10;
    time = 0;
    time2 = 0;
else
    % Change directory to cygwin bin
    old = cd('C:\cygwin64\bin');
    [status, result] = system(['bash --login ' old '\batteryReport.sh']);
    % In Window this only updates every 5 min
    cd(old);

    % Parse result for time and cap
    %result
    token = strread(result, '%s');

    % Window battery_report timer, only update every 5 min
    %time2 = token{8};
    %[hour, minute, second] = strread(token{8},'%d:%d:%d');
    % System clock, more precise
    c = clock; time2 = sprintf('%.2d:%.2d:%.2d', c(4), c(5), round(c(6)));
    hour = c(4); minute = c(5); second = round(c(6));

    time = hour*60^2 + minute*60 + second;
    cap = strread(token{end-1}(token{end-1} ~= ','), '%d'); % remove extra ','
end