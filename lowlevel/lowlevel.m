% Low level component of the acoustic search system
% 
% instead of recording from the computer's mic (as in the demo in the 
% longRecord branch), this version uses a 20 minute wave file as a test
% vector. this is useful for verifying correct functionality.
%
% all signal processing has been placed in the audio callback function,
% with the main loop reserved for POSTing events.
%
% David Jun
% University of Illinois
% 05/08/2014
%
% Add timer callback to POST status periodically.
%
% Long Le, longle1@illinois.edu
% University of Illinois
% 05/18/2014
%
% Add code to use LIVESTREAMING flag to choose MIC over testvector.
%
% David Jun 05/25/2014

clear all; close all;
addpath('C:\Users\long\Desktop\projects\acousticsearch\common');
%addpath('C:\Users\long\Desktop\projects\cygwin\msub');

% Clear deprecated timer or instr object
delete(timerfind);
delete(instrfind);

LIVESTREAMING = 1;
%% Parameters

% "play" audio wave file in blocks of 100ms
global rec Nblk fs

if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
    % Configure audio recorder
    fs = 16000;
    Nblk = 512;
    rec = dsp.AudioRecorder('SampleRate',fs,'NumChannels',1,'BufferSizeSource','Property','BufferSize',8192,...
        'QueueDuration',2,'SamplesPerFrame',Nblk);
    timerDSP = timer('TimerFcn',@timerDSP_cb, 'Period', Nblk/fs, 'ExecutionMode','fixedRate');
else
%    [rec, fs] = audioread('../data/clean_data_15secnoise_48khz.wav');
    [rec, fs] = audioread('../data/whistlenoisedata.wav');
    rec = resample(rec,8000,fs);
    fs = 8000;
    Nblk = 1024;
    timerAudio = timer('TimerFcn',@timerAudioFFT_cb, 'Period', Nblk/fs, 'ExecutionMode','fixedRate');
end

global idx done
idx = 1; % index of current sample in the audio queue
done = false; % indicator of audio record ending (only used for testvector)

% Configure TCPIP
global recvTCP
recvTCP = tcpip('localhost', 8086); % remote host and port
recvTCP.InputBufferSize = 2^16;
recvTCP.BytesAvailableFcnMode = 'terminator';
recvTCP.Terminator = 'LF';
recvTCP.BytesAvailableFcn = @recvTCP_cb;

% Configure timer POST
timerPOST = timer('TimerFcn',@timerPOST_cb, 'Period', 6*60, 'ExecutionMode','fixedRate');

% Configure timer GET
timerGET = timer('TimerFcn',@timerGET_cb, 'Period', 10, 'ExecutionMode','fixedRate');

%% Processing parameters
global Nfc
Nfc = 256;

% noise tracking parameters
global noisetracker
alpha_updown = 0.01;
slow_scale = 0.1;
noisetracker.indicator_countdown = 10;
noisetracker.floor_up = (1+alpha_updown)*ones(Nfc,1);
noisetracker.floor_up_slow = (1+slow_scale*alpha_updown)*ones(Nfc,1);
noisetracker.floor_down = (1-alpha_updown)*ones(Nfc,1);
noisetracker.floor_thresh = 0.1*ones(Nfc,1);
noisetracker.det_thresh_scale = 4.0*ones(Nfc,1);
noisetracker.indicator_last = zeros(Nfc,1);

% Ridge tracking parameter
global ridgetracker

% Parameters
ridgetracker.alpha_ridge = exp(-Nblk/(0.1*fs));
ridgetracker.f_ridge_down_max = ceil(20000*Nblk*Nfc*2/(fs*fs)); % want X Hz/sec chirp rate; f_ridge_down_max units are freq-samples/blklen-time-samples; 1 freq-sample = fs/binct Hz
ridgetracker.f_ridge_up_max = ridgetracker.f_ridge_down_max;
ridgetracker.f_ridge_down = zeros(Nfc,1);
ridgetracker.f_ridge_up = zeros(Nfc,1);
for ii=1:Nfc,
   ridgetracker.f_ridge_down(ii) = max(1,ii-ridgetracker.f_ridge_down_max);
   ridgetracker.f_ridge_up(ii) = min(Nfc,ii+ridgetracker.f_ridge_up_max);
end
ridgetracker.ridge_offset_penalty_max = 0.05;
ridgetracker.ridge_offset_penalty = ridgetracker.ridge_offset_penalty_max/max(ridgetracker.f_ridge_up_max,ridgetracker.f_ridge_down_max);
ridgetracker.log_prob_noise_min_ridge = -6.0;
ridgetracker.detect_ridge_backtrack_thresh = -4.0;
ridgetracker.backtracklen = ceil(0.05*fs/Nblk); % enter the multiplier as the approximate track length in sec
% States
ridgetracker.log_prob_noise_ridge_cum_now = zeros(Nfc,1);
ridgetracker.specridge_link_back = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.speclog_prob_noise_ridge = zeros(Nfc,ridgetracker.backtracklen+1);
ridgetracker.stftdetectridgeback = zeros(Nfc,2);
ridgetracker.ridge_min_time = zeros(1,0);
ridgetracker.ridge_max_time = zeros(1,0);
ridgetracker.ridge_min_freq = zeros(1,0);
ridgetracker.ridge_max_freq = zeros(1,0);
ridgetracker.ridge_loglik = zeros(1,0);
ridgetracker.ridge_desc = cell(1,0);
ridgetracker.ridgect = 0;

% INFORMATION FROM HIGH-LEVEL TASK... only process 9th band
global bnd thresh
bnd = 1:4;
thresh = -500.0;

global pktQ
pktQ = CQueue();

winlen = 150;
global tBuf sBuf rBuf figH
tBuf = zeros(1, winlen);
sBuf = zeros(Nfc, winlen);
rBuf = cell(1, winlen);
for k = 1:winlen
    rBuf{k} = cell(0);
end
figH = figure();

global t2Buf s2Buf
t2Buf = zeros(50,1);
if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
    s2Buf = zeros(50,Nfc);
else
    s2Buf = zeros(50,1);
end

%% Main 

% start audio playback timer
if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
    start(timerDSP);
else
    start(timerAudio);
end

% Start tcp receive callback
fopen(recvTCP);
[cap, time] = batteryReport; % Setup contract based on battery capacity
msg = sprintf(['POST /rab/owner/contract?capacity=%d&time=%d&lifetime=%d HTTP/1.0' 10 'Connection: Keep-Alive' 10 10], cap, time, 3600);
fprintf(recvTCP, msg);

% start periodic status post, need recvTCP open first
start(timerPOST);
start(timerGET);

while(1)
    % POST!
    if(~isempty(pktQ))
        for p=1:size(pktQ)
            pkt = pktQ.pop();
            msg = sprintf(['POST /rab/owner/stream?loglik=%1.6f&duration=%1.3f&bw=%1.0f&time=%1.3f&freq=%1.0f HTTP/1.0' 10 'Connection: Keep-Alive' 10 10], ...
                pkt.loglik, pkt.duration, pkt.bw, pkt.time, pkt.freq);
            %fprintf(recvTCP, msg); % icinterface/fprintf does not support runtime formatted message.
            display(msg);
        end
    end
    if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
    else
        drawnow;
    end
end

if(exist('LIVESTREAMING','var') && LIVESTREAMING==1)
    stop(timerDSP); delete(timerDSP); release(rec);
else
    stop(timerAudio); delete(timerAudio);
end
stop(timerPOST); delete(timerPOST);
stop(timerGET); delete(timerGET);
fclose(recvTCP); delete(recvTCP);
