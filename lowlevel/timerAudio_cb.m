% timerAudio_cb.m
%
% "acquires" a block of audio data. this callback is controlled by a timer,
% which is operating in real time, but can be sped up to run through data
% faster.
%
% low-level processing and event generation/construction happens here.
%
% David Jun
% University of Illinois
% 05/08/2014
%
% changed testVec to rec to match real-time audio streaming.
%
% David Jun 05/25/2014

function timerAudio_cb(obj, event)

global idx rec Nblk done 

global figH tBuf sBuf s2Buf t2Buf

global Hd noisetracker pktQ bnd

band = bnd;
thresh = noisetracker.det_thresh_scale;

%disp('In the callback.')
if (idx >= length(rec)-Nblk)
%if(idx >= 30*48000)
%    done = true;   % quits when file is over
    idx = 1;        % starts all over again!
    return;
end
data = rec(idx:idx+Nblk-1);

% DEBUG plot "live" data
tBuf = [tBuf(Nblk+1:end); (idx:idx+Nblk-1)'];
sBuf = [sBuf(Nblk+1:end); data];
figure(figH);
subplot(2,1,1);
plot(tBuf, sBuf);
axis([tBuf(1), tBuf(end), -0.1 0.1]);

% increment index pointer
idx = idx + Nblk;

% LOW LEVEL processing
curevt = zeros(1, numel(noisetracker.floor_thresh));
for jj=1:length(band)
    j = band(jj);

    % compute test statistic
    s = filter(Hd(j),data);
    y = norm(s);

    % noise tracking
    if(y > noisetracker.floor_thresh(j))
        noisetracker.indicator_last(j) = noisetracker.indicator_last(j)-1;
        if(noisetracker.indicator_last(j) < 0) % signal is highly probable, slow down
            noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up_slow(j);
        else % transient signal is highly probable
            noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_up(j);
        end
    else % background noise is highly probable
        noisetracker.indicator_last(j) = noisetracker.indicator_countdown;
        noisetracker.floor_thresh(j) = noisetracker.floor_thresh(j)*noisetracker.floor_down(j);
    end
    % get labels (via declaring detection)
    if(y > thresh(j)*noisetracker.floor_thresh(j))
        curevt(1,j) = 1;
        % this is where we would POST an event to high-level search
        % include: 
        %   freq band       j
        %   timestamp       i
        %   sig power       y(j,i)
        %   noise power     floor_thresh(j)
        pkt = [];
        pkt.freq  = j;
        pkt.time  = (idx-1)/Nblk;
        pkt.sig   = y;
        pkt.noise = noisetracker.floor_thresh(j);

        pktQ.push(pkt);
    end
end

t2Buf = [t2Buf(2:end); (idx-1)/Nblk];
s2Buf = [s2Buf(2:end,:); curevt];
% find all bands that are activated in this window
msg = sprintf('Activated bands: ');
for j=1:10
    if(sum(s2Buf(:,j)) > 0)
        msg = [msg, sprintf('%i ',j)];
    end
end
subplot(2,1,2);
plot(t2Buf, s2Buf);
title(msg);
axis([t2Buf(1) t2Buf(end) -0.5 1.5]);
% for i=1:10
%     subplot(11,1,i+1);
% %    plot(t2Buf,s2Buf(:,i));
%     plot(s2Buf(:,i));
% end
%     axis([t2Buf(1) t2Buf(end) -0.5 1.5]);
